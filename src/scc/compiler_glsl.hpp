#pragma once

#include <SPIRV-Cross/spirv_glsl.hpp>

#include "core/reader.hpp"

#include "gfx/format.hpp"

namespace kr {
namespace scc {

enum class Semantic {
	Position,
	Normal,
	TexCoord,
	Material,
	Color,
	Tangent,
	Binormal,
	Unknown
};

struct Type {
	int size;
	const char* name;
	gfx::Format format;
};

struct Resource {
	std::string name;
	int id;
	Type type;
	Semantic semantic = Semantic::Unknown;
	int location = -1;
	int set = -1;
	int binding = -1;
	int attachment = -1;
	int count = -1;
	int offset = 0;
	int size = 0;
	bool readonly = false;
	bool writeonly = false;
	std::vector<Resource> members;
};

struct Reflection {
	std::vector<Resource> input;
	std::vector<Resource> uniform;
	std::vector<Resource> samplers;
};

class CompilerGLSL {
public:
	typedef spirv_cross::CompilerGLSL::Options Options;

	CompilerGLSL(std::vector<uint32_t> spirv);
	~CompilerGLSL();

	Reflection reflect();
	std::string compile(const Options& options);

private:
	enum class ResourceType {
		UniformBuffer,
		StorageBuffer,
		StageInput,
		StageOutput,
		SubpassInput,
		StorageImage,
		SampledImage,
		AtomicCounter,
		AccelerationStructure,
		PushConstantBuffer,
		SeparateImage,
		SeparateSampler
	};

	Type get_type(const spirv_cross::SPIRType& type);
	void reflect_resource(const spirv_cross::SmallVector<spirv_cross::Resource>& ress, std::vector<Resource>& reflection, ResourceType restype);

	Reader* p_reader;
	std::vector<uint32_t> m_spirv;
	spirv_cross::CompilerGLSL* p_glsl;
};

}}
