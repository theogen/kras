#pragma once

#include <string>
#include <vector>

namespace kr {

class Filesys {
public:
	static std::string cwd();
	static bool chdir(const std::string& dir);

	static bool is_regular_file(const std::string& path);
	static bool is_directory(const std::string& path);

	static bool file_exists(const std::string& path);

	/* Path operations */

	static std::string basename(const std::string& path);
	static std::string dirname(const std::string& path, bool trailing_slash = false);

	static std::string suffix(const std::string& path, bool lower = true);
	static std::string remove_suffix(const std::string& path);

	static std::vector<char>* istream_to_char_arr(std::istream& in, size_t block_size = 4096);

	/* Modifications */

	static bool mkdir(const std::string& path);
};

}
