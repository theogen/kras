#pragma once

#include "opengl.h"
#include "../../format.hpp"

namespace kr {
namespace gfx {
namespace gl {

struct FormatInfo {
	int format;
	int intern_format;
	unsigned size;
	GLenum type;
	bool normalized;

	FormatInfo() {}
	FormatInfo(
		int format,
		int intern_format,
		unsigned size,
		GLenum type,
		bool normalized
	) :
		format(format),
		intern_format(intern_format),
		size(size),
		type(type),
		normalized(normalized)
	{}
};

FormatInfo get_format_info(Format format);
int get_gl_format(Format format);

}}}
