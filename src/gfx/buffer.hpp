#pragma once

#include <string>

namespace kr {
namespace gfx {

enum class BufferUsage {
    Index,
    Vertex,
    Uniform
};

class Buffer {
public:
	struct Info {
		std::string name;
		BufferUsage usage;
		uint32_t size;
		const void* data = nullptr;
	};

	virtual ~Buffer() {}

	virtual void write(
		void* data,
		size_t size = 0,
		size_t offset = 0
	) = 0;

	virtual uint32_t size() = 0;
};

}}
