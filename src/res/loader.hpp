#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <SDL2/SDL.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>

#include "gfx/format.hpp"
#include "gfx/device.hpp"

#include "core/reader.hpp"

struct lua_State;

namespace kr {
namespace res {

class Loader;
class OBJLoader;

class Resource {
public:
	std::string path;
	virtual ~Resource() {}
};

struct Model : public Resource {
	struct Vertex {
		glm::vec3 pos;
		glm::vec3 normal;
		glm::vec2 texcoord;
		glm::vec3 color;
		float material;

		bool operator==(const Vertex& other) const;
	};

	struct Material {
		std::string name;
		glm::vec3 diffuse;
	};

	std::vector<uint32_t> indices;
	std::vector<Vertex> vertices;
	std::vector<Material> materials;
};

class Loader {
public:
	Loader(Reader* reader, gfx::Device* device);
	~Loader();

	gfx::Image* image(const std::string& path);
	Model model(const std::string& path);
	gfx::Pipeline* pipeline(const std::string& path);

private:
	lua_State* lua_open(const std::string& path);

	Reader* p_reader;
	gfx::Device* p_device;
	OBJLoader* p_obj_loader;

	std::unordered_map<std::string, gfx::Image*> m_images;
	std::unordered_map<std::string, gfx::Pipeline*> m_pipelines;
};


class ProjectFileReader : public FileReader {
public:
	ProjectFileReader(const std::string project_path);

	virtual std::string resolve(const std::string& path) const override;

protected:
	std::string m_project_path;
};

}} // namespace kr::res


template <class T>
inline void hash_combine(std::size_t& seed, const T& v)
{
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

namespace std {
    template<>
	struct hash<kr::res::Model::Vertex> {
		size_t operator()(kr::res::Model::Vertex const& vertex) const
		{
			size_t hash = 0;
			hash_combine<glm::vec3>(hash, vertex.pos);
			hash_combine<glm::vec3>(hash, vertex.normal);
			hash_combine<glm::vec2>(hash, vertex.texcoord);
			hash_combine<glm::vec3>(hash, vertex.color);
			hash_combine<float>(hash, vertex.material);
#if 0
			size_t h1 = hash<glm::vec3>()(vertex.pos);
			size_t h2 = hash<glm::vec3>()(vertex.normal);
			size_t h3 = hash<glm::vec2>()(vertex.texcoord);
			size_t h4 = hash<glm::vec3>()(vertex.color);
			size_t h5 = hash<uint32_t>()(vertex.material);
			return ((h1 ^ (h2 << 1)) >> 1) ^ (h3 << 1);
#endif
			return hash;
		}
    };
}

