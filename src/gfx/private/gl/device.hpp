#pragma once

#include <SDL2/SDL.h>

#include "gfx/device.hpp"

namespace kr {
namespace gfx {
namespace gl {

class Device : public gfx::Device {
public:
	Device(const Info& info);
	~Device();

	virtual gfx::Pipeline* create_pipeline(const gfx::Pipeline::Info& info) override;
	virtual gfx::Buffer* create_buffer(const gfx::Buffer::Info& info) override;
	virtual gfx::Image* create_image(const gfx::Image::Info& info) override;

	virtual void add_input(gfx::Input* input) override;

	virtual void render() override;

	virtual bool vsync() const override { return m_vsync; }
	virtual void set_vsync(bool state) override;

private:
	void set_attributes();
	void create_context();

private:
	SDL_Window* p_window;
	SDL_GLContext m_context;
	int m_msaa;
	bool m_vsync;

	std::vector<gfx::Input*> m_inputs;
};

}}}
