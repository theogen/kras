#include <iostream>

#include <cstdio>
#include <cstdlib>
#include <ctime>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <core/cli.hpp>
#include <core/time.hpp>
#include <gfx/gfx.hpp>
#include <res/res.hpp>
#include <event/event_collector.hpp>
#include <node/node.hpp>
#include <node/mesh.hpp>
#include <node/camera.hpp>
#include <renderer/mesh.hpp>

struct Options {
	kr::gfx::Driver driver;
};

class App {
public:
	App(const Options& options) : m_options(options) {}

	void init()
	{
		init_context();

		kr::node::Mesh::Info mesh;
		mesh.model = p_loader->model("models/craft_speederA.obj");
		mesh.pipeline = p_loader->pipeline("default.pipeline");
		p_ship = new kr::node::Mesh("player", p_renderer, mesh);
		p_ship->set_position(glm::vec3(0, 0, 15));
		p_ship->set_rotation(glm::vec3(0, 180, 0));

		mesh.model = p_loader->model("models/rocket_topA.obj");
		mesh.pipeline = p_loader->pipeline("default.pipeline");
		p_obstacle = new kr::node::Mesh("meteor", p_renderer, mesh);
		reset_obstacle();

		p_camera = new kr::node::Camera("camera");
		p_camera->set_aspect(1.6f);
		p_camera->set_fov(60);
		p_camera->set_near(0.1f);
		p_camera->set_far(100.0f);
		p_camera->set_position(glm::vec3(0, 10, -30));
		p_camera->rotate(glm::vec3(20, 0, 0));
		p_camera->recalculate_projection_matrix();

		p_renderer->set_camera(p_camera);
	}

	void reset_obstacle()
	{
		srand((unsigned) time(0));
		p_obstacle->set_position(glm::vec3(1 + (rand() % 20) - 10, 0, -10));
		p_obstacle->set_scale(glm::vec3(5, 5, 5));
		p_obstacle->set_rotation(glm::vec3(90, 0, 0));
	}

	void run()
	{
		kr::Time time;
		while (!p_event->is_close_requested()) {
			p_event->poll();
			time.update();

			if (p_event->state(kr::event::Scancode::Left)) {
				p_ship->translate(glm::vec3(-1, 0, 0), time.delta());
				p_ship->set_rotation(glm::vec3(0, 180, -10));
			}
			else if (p_event->state(kr::event::Scancode::Right)) {
				p_ship->translate(glm::vec3(1, 0, 0), time.delta());
				p_ship->set_rotation(glm::vec3(0, 180, 10));
			}
			else {
				p_ship->set_rotation(glm::vec3(0, 180, 0));
			}

			p_obstacle->translate(glm::vec3(0, 0, 1), time.delta());
			if (p_obstacle->position().z > 25)
				reset_obstacle();

			p_ship->update();
			p_obstacle->update();

			p_device->render();
		}
	}

private:
	void init_context()
	{
		kr::gfx::Device::Info device;
		{
			device.driver = m_options.driver;
			device.window.title = "Ship";
			device.window.w = 1280;
			device.window.h = 768;
			device.window.x = 0;
			device.window.y = 0;
			device.window.resizable = true;
			device.msaa = 8;
			device.vsync = false;
		}
		p_device = kr::gfx::create_device(device);

		p_reader = new kr::res::ProjectFileReader("./sample/ship");

		p_loader = new kr::res::Loader(p_reader, p_device);

		p_event = new kr::event::EventCollector();

		kr::renderer::MeshRenderer::Info renderer;
		{
			renderer.device = p_device;
			renderer.loader = p_loader;
		}
		p_renderer = new kr::renderer::MeshRenderer(renderer);
	}

private:
	Options m_options;

	kr::gfx::Device* p_device;
	kr::event::EventCollector* p_event;
	kr::res::ProjectFileReader* p_reader;
	kr::res::Loader* p_loader;

	kr::renderer::MeshRenderer* p_renderer;
	kr::node::Mesh* p_ship;
	kr::node::Mesh* p_obstacle;
	kr::node::Camera* p_camera;
};

int main(int argc, const char** argv)
{
	kr::CLI cli(argc, argv, { "driver" });
	Options options;
	options.driver = kr::gfx::Driver::OpenGL;
	if (cli.option("driver") == "vk")
		options.driver = kr::gfx::Driver::Vulkan;
	App app(options);

	try {
		app.init();
		app.run();
	}
	catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}


	return EXIT_SUCCESS;
}

