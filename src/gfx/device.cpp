#include <SDL2/SDL.h>

#include "device.hpp"

namespace kr {
namespace gfx {

void Device::init_sdl()
{
	// Initialize video.
	int res = SDL_Init(SDL_INIT_VIDEO);
	if (res != 0) {
		throw std::runtime_error(
			std::string("Failed to initialize SDL: ") + SDL_GetError()
		);
	}
}

SDL_Window* Device::create_window(const WindowInfo& info, int flags)
{
	// Flags.
	if (info.fullscreen)         flags |= SDL_WINDOW_FULLSCREEN;
	if (info.fullscreen_desktop) flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	if (info.hidden)             flags |= SDL_WINDOW_HIDDEN;
	if (info.borderless)         flags |= SDL_WINDOW_BORDERLESS;
	if (info.resizable)          flags |= SDL_WINDOW_RESIZABLE;
	if (info.minimized)          flags |= SDL_WINDOW_MINIMIZED;
	if (info.maximized)          flags |= SDL_WINDOW_MAXIMIZED;
	if (info.input_grab)         flags |= SDL_WINDOW_INPUT_GRABBED;
	if (info.highdpi)            flags |= SDL_WINDOW_ALLOW_HIGHDPI;

	// Create window.
	SDL_Window* window = SDL_CreateWindow(
		info.title,
		info.x, info.y,
		info.w, info.h,
		flags
	);
	if (!window) {
		throw std::runtime_error(
			std::string("Couldn't create window: ") + SDL_GetError()
		);
	}

	return window;
}

}}
