#pragma once

#include <unordered_map>

#include "node.hpp"
#include "gfx/device.hpp"
#include "renderer/mesh.hpp"
#include "res/res.hpp"

namespace kr {
namespace node {

class Mesh : public Node {
public:
	struct Info {
		res::Model model;
		gfx::Pipeline* pipeline;
	};

	Mesh(const std::string& name, renderer::MeshRenderer* renderer, const Info& info);
	~Mesh();

	virtual void update() override;
	virtual void set_active(bool active) override;

	void uniform(int index, void* data, size_t size = 0, size_t offset = 0);
	void uniform(const std::string& name, void* data, size_t size = 0, size_t offset = 0);
	void uniform(int index, gfx::Image* image);
	void uniform(const std::string& name, gfx::Image* image);

private:
	void initialize();

	gfx::Input* p_input;
	gfx::Device* p_device;
	gfx::Pipeline* p_pipeline;
	renderer::MeshRenderer* p_renderer;
	res::Model m_model;

	int m_transform_ubo_id = -1;
	int m_camera_ubo_id = -1;
	int m_mvp_offset = -1;
	int m_model_offset = -1;
	int m_view_offset = -1;
	int m_proj_offset = -1;
};

}}
