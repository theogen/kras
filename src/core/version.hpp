#pragma once

namespace kr {

// Semantic version representation.
struct Version {
	int major;
	int minor;
	int patch;

	Version() {}
	Version(int major, int minor, int patch)
	{
		this->major = major;
		this->minor = minor;
		this->patch = patch;
	}
};

}
