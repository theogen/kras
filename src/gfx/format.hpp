#pragma once

#include <cstdint>

namespace kr {
namespace gfx {

/* Format */

enum class Format {

	Unknown,

	/* R */

	R8UN,
	R8SN,
	R8SI,
	R8UI,

	R16UN,
	R16SN,
	R16SI,
	R16UI,
	R16SF,

	R32SI,
	R32UI,
	R32SF,

	/* RG */

	RG8UN,
	RG8SN,
	RG8SI,
	RG8UI,

	RG16UN,
	RG16SN,
	RG16SI,
	RG16UI,
	RG16SF,

	RG32SI,
	RG32UI,
	RG32SF,

	/* RGB */

	RGB8UN,
	RGB8SN,
	RGB8SI,
	RGB8UI,

	RGB16UN,
	RGB16SN,
	RGB16SI,
	RGB16UI,
	RGB16SF,

	RGB32SI,
	RGB32UI,
	RGB32SF,

	/* RGBA */

	RGBA8UN,
	RGBA8SN,
	RGBA8SI,
	RGBA8UI,

	RGBA16UN,
	RGBA16SN,
	RGBA16SI,
	RGBA16UI,
	RGBA16SF,

	RGBA32SI,
	RGBA32UI,
	RGBA32SF,

	/* BGR */

	BGR8UN,
	BGR8SN,
	BGR8UI,
	BGR8SI,

	/* BGRA */

	BGRA8UN,
	BGRA8SN,
	BGRA8UI,
	BGRA8SI,

	/* Depth */

	D16,
	D24S8,
	D32S8,
	D32SF
};

Format format_from_sdl(uint32_t format);

}}
