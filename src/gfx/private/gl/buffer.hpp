#pragma once

#include "opengl.h"
#include "../../buffer.hpp"

namespace kr {
namespace gfx {
namespace gl {

// Manages a buffer or multiple identical buffers.
class Buffer : public gfx::Buffer {
public:
	Buffer(const Info& info);
	virtual ~Buffer();

	virtual void write(
		void* data,
		size_t size,
		size_t offset
	) override;

	virtual uint32_t size() override { return m_size; }

	unsigned id() const noexcept { return m_buffer; }
	
private:
	uint32_t m_size;
	unsigned m_buffer;
	GLenum m_target;
};

}}}
