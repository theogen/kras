#include <stdexcept>

#include "opengl.h"
#include "device.hpp"
#include "pipeline.hpp"
#include "buffer.hpp"
#include "image.hpp"

namespace kr {
namespace gfx {
namespace gl {

Device::Device(const Info& info)
	: m_msaa(info.msaa)
{
	init_sdl();
	set_attributes();
	p_window = create_window(info.window, SDL_WINDOW_OPENGL);
	create_context();
	glEnable(GL_DEPTH_TEST);
	if (m_msaa > 0)
		glEnable(GL_MULTISAMPLE);

	set_vsync(info.vsync);
}

Device::~Device()
{
	SDL_GL_DeleteContext(m_context);
	SDL_DestroyWindow(p_window);
	SDL_Quit();
}

gfx::Pipeline* Device::create_pipeline(const gfx::Pipeline::Info& info)
{
	return new Pipeline(info, this);
}

gfx::Buffer* Device::create_buffer(const gfx::Buffer::Info& info)
{
	return new Buffer(info);
}

gfx::Image* Device::create_image(const gfx::Image::Info& info)
{
	return new Image(info);
}

void Device::add_input(gfx::Input* input)
{
	m_inputs.push_back(input);
}

void Device::render()
{
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (const Input* input : m_inputs) {
		if (!input->active)
			continue;
		Pipeline* pipeline = dynamic_cast<Pipeline*>(input->pipeline);
		pipeline->bind();
		pipeline->bind_index_buffer(input->indices, IndexType::UInt32);
		pipeline->bind_vertex_buffer(input->vertices);
		pipeline->bind_descriptors(input->descriptors);
		pipeline->draw(input->indices->size() / sizeof(uint32_t));
	}

	SDL_GL_SwapWindow(p_window);
}

void Device::set_vsync(bool state)
{
	if (state) {
		int success = SDL_GL_SetSwapInterval(-1);
		if (success == -1)
			SDL_GL_SetSwapInterval(1);
	}
	else {
		SDL_GL_SetSwapInterval(0);
	}
	m_vsync = state;
}

void Device::set_attributes()
{
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	if (m_msaa > 0) {
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, m_msaa);
	}
}

void Device::create_context()
{
	m_context = SDL_GL_CreateContext(p_window);

	if (m_context == nullptr) {
		throw std::runtime_error("Couldn't create OpenGL context!\n");
	}

#ifndef __APPLE__
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		throw std::runtime_error("Couldn't initialize GLEW!\n");
	}
	if (!GLEW_VERSION_2_1) {
		throw std::runtime_error("GLEW 2.1 required!\n");
	}
#endif
}

}}}
