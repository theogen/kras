#pragma once

#include <unordered_map>

#include "buffer.hpp"
#include "image.hpp"

namespace kr {
namespace gfx {

struct Descriptor {
	Buffer* buffer = nullptr;
	Image* image = nullptr;
	bool is_buffer;
	bool is_image;

	Descriptor() {}
	Descriptor(Buffer* buffer)
		: buffer(buffer), is_buffer(true), is_image(false) {}
	Descriptor(Image* image)
		: image(image), is_buffer(false), is_image(true) {}
};

struct Input {
	Pipeline* pipeline;
	Buffer* indices;
	Buffer* vertices;
	std::unordered_map<int, Descriptor> descriptors;
	bool active = true;
};

}}
