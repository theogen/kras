#include <iostream>
#include <string>

#include <SDL2/SDL.h>

#include "format.hpp"

namespace kr {
namespace gfx {

Format format_from_sdl(uint32_t format)
{
	std::string name;
	switch (format) {
		case SDL_PIXELFORMAT_RGB888:
			return Format::RGB8UN;
		case SDL_PIXELFORMAT_BGR24:
		case SDL_PIXELFORMAT_BGR888:
			return Format::BGR8UN;
		case SDL_PIXELFORMAT_RGBA8888:
		case SDL_PIXELFORMAT_RGBA32:
			return Format::RGBA8UN;
		case SDL_PIXELFORMAT_BGRA8888:
			return Format::BGRA8UN;
		case SDL_PIXELFORMAT_UNKNOWN:
			name = "SDL_PIXELFORMAT_UNKNOWN";
			break;
		case SDL_PIXELFORMAT_INDEX1LSB:
			name = "SDL_PIXELFORMAT_INDEX1LSB";
			break;
		case SDL_PIXELFORMAT_INDEX1MSB:
			name = "SDL_PIXELFORMAT_INDEX1MSB";
			break;
		case SDL_PIXELFORMAT_INDEX4LSB:
			name = "SDL_PIXELFORMAT_INDEX4LSB";
			break;
		case SDL_PIXELFORMAT_INDEX4MSB:
			name = "SDL_PIXELFORMAT_INDEX4MSB";
			break;
		case SDL_PIXELFORMAT_INDEX8:
			name = "SDL_PIXELFORMAT_INDEX8";
			break;
		case SDL_PIXELFORMAT_RGB332:
			name = "SDL_PIXELFORMAT_RGB332";
			break;
		case SDL_PIXELFORMAT_RGB444:
			name = "SDL_PIXELFORMAT_RGB444";
			break;
		case SDL_PIXELFORMAT_RGB555:
			name = "SDL_PIXELFORMAT_RGB555";
			break;
		case SDL_PIXELFORMAT_BGR555:
			name = "SDL_PIXELFORMAT_BGR555";
			break;
		case SDL_PIXELFORMAT_ARGB4444:
			name = "SDL_PIXELFORMAT_ARGB4444";
			break;
		case SDL_PIXELFORMAT_RGBA4444:
			name = "SDL_PIXELFORMAT_RGBA4444";
			break;
		case SDL_PIXELFORMAT_ABGR4444:
			name = "SDL_PIXELFORMAT_ABGR4444";
			break;
		case SDL_PIXELFORMAT_BGRA4444:
			name = "SDL_PIXELFORMAT_BGRA4444";
			break;
		case SDL_PIXELFORMAT_ARGB1555:
			name = "SDL_PIXELFORMAT_ARGB1555";
			break;
		case SDL_PIXELFORMAT_RGBA5551:
			name = "SDL_PIXELFORMAT_RGBA5551";
			break;
		case SDL_PIXELFORMAT_ABGR1555:
			name = "SDL_PIXELFORMAT_ABGR1555";
			break;
		case SDL_PIXELFORMAT_BGRA5551:
			name = "SDL_PIXELFORMAT_BGRA5551";
			break;
		case SDL_PIXELFORMAT_RGB565:
			name = "SDL_PIXELFORMAT_RGB565";
			break;
		case SDL_PIXELFORMAT_BGR565:
			name = "SDL_PIXELFORMAT_BGR565";
			break;
		case SDL_PIXELFORMAT_RGB24:
			name = "SDL_PIXELFORMAT_RGB24";
			break;
		case SDL_PIXELFORMAT_RGBX8888:
			name = "SDL_PIXELFORMAT_RGBX8888";
			break;
		case SDL_PIXELFORMAT_BGRX8888:
			name = "SDL_PIXELFORMAT_BGRX8888";
			break;
		case SDL_PIXELFORMAT_ARGB8888:
			name = "SDL_PIXELFORMAT_ARGB8888";
			break;
		case SDL_PIXELFORMAT_ARGB2101010:
			name = "SDL_PIXELFORMAT_ARGB2101010";
			break;
		case SDL_PIXELFORMAT_YV12:
			name = "SDL_PIXELFORMAT_YV12";
			break;
		case SDL_PIXELFORMAT_IYUV:
			name = "SDL_PIXELFORMAT_IYUV";
			break;
		case SDL_PIXELFORMAT_YUY2:
			name = "SDL_PIXELFORMAT_YUY2";
			break;
		case SDL_PIXELFORMAT_UYVY:
			name = "SDL_PIXELFORMAT_UYVY";
			break;
		case SDL_PIXELFORMAT_YVYU:
			name = "SDL_PIXELFORMAT_YVYU";
			break;
		case SDL_PIXELFORMAT_NV12:
			name = "SDL_PIXELFORMAT_NV12";
			break;
		case SDL_PIXELFORMAT_NV21:
			name = "SDL_PIXELFORMAT_NV21";
			break;
	}

	std::cout << name << "\n";
	throw std::runtime_error("Unsupported pixel format.");
	return Format::Unknown;
}

}}
