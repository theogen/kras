#include <iostream>

#include <glslang/SPIRV/GlslangToSpv.h>

#include "compiler_spirv.hpp"

#include "private/limits.hpp"
#include "private/includer.hpp"

namespace kr {
namespace scc {

const char* k_semantic_names[] = {
	"POSITION",
	"NORMAL",
	"TEXCOORD",
	"MATERIAL",
	"COLOR",
	"TANGENT",
	"BINORMAL"
};

bool CompilerSPIRV::glslang_initialized = false;

CompilerSPIRV::CompilerSPIRV(Reader* reader)
	: p_reader(reader)
{
	// Initialize glslang.
	if (!glslang_initialized)
	{
		glslang::InitializeProcess();
		glslang_initialized = true;
	}
}

static std::string dirname(const std::string& str)
{
	size_t found = str.find_last_of("/\\");
	return str.substr(0, found);
}

static std::string suffix(const std::string& name)
{
	const size_t pos = name.rfind('.');
	return (pos == std::string::npos) ? "" : name.substr(pos + 1);
}

std::vector<uint32_t> CompilerSPIRV::compile(const std::string& path)
{
	// Getting source.
	p_reader->open(path.c_str());
	char* source = new char[p_reader->length() + 1];
	p_reader->read(source);
	source[p_reader->length()] = '\0';
	p_reader->close();

	gfx::Stage stage;
	if (suffix(path) == "vert")
		stage = gfx::Stage::Vertex;
	if (suffix(path) == "frag")
		stage = gfx::Stage::Fragment;

	auto result = compile(source, stage, path);

	delete [] source;

	return result;
}


std::vector<uint32_t> CompilerSPIRV::compile(const char* source, gfx::Stage stage, const std::string& path)
{
	EShLanguage shader_type = get_stage(stage);
	glslang::TShader shader(shader_type);
	std::string src = std::string("#version 450\n") + source; 
	const char* csrc = src.c_str();
	shader.setStrings(&csrc, 1);

	// Set up Vulkan/Spir-V environment.
	int client_input_semantics_version = 100; // maps to, say, #define VULKAN 100
	glslang::EShTargetClientVersion vulkan_client_version = glslang::EShTargetVulkan_1_0;  // would map to, say, Vulkan 1.0
	glslang::EShTargetLanguageVersion target_version = glslang::EShTargetSpv_1_0;	// maps to, say, SPIR-V 1.0

	shader.setEnvInput(glslang::EShSourceGlsl, shader_type, glslang::EShClientVulkan, client_input_semantics_version);
	shader.setEnvClient(glslang::EShClientVulkan, vulkan_client_version);
	shader.setEnvTarget(glslang::EShTargetSpv, target_version);

	// construct semantics mapping defines
	// to be used in layout(location = SEMANTIC) inside GLSL
	std::string def;
	for (unsigned i = 0; i < sizeof(k_semantic_names) / sizeof(k_semantic_names[0]); ++i) {
		def += std::string("#define ") + k_semantic_names[i] + " " + std::to_string(i) + "\n";
	}
	shader.setPreamble(def.c_str());

	TBuiltInResource resources = k_default_limits;
	EShMessages messages = (EShMessages) (EShMsgSpvRules | EShMsgVulkanRules);

	const int default_version = 100;

	DirStackFileIncluder includer(p_reader);
	
	// Get directory.
	std::string dir = dirname(path);
	includer.push_extern_local_dir(dir);

	std::string preprocessed_glsl;

	if (!shader.preprocess(&resources, default_version, ENoProfile, false, false, messages, &preprocessed_glsl, includer)) 
	{
		std::cout << "GLSL Preprocessing Failed for: " << path << std::endl;
		std::cout << shader.getInfoLog() << std::endl;
		std::cout << shader.getInfoDebugLog() << std::endl;
	}

	const char* preprocessed_cstr = preprocessed_glsl.c_str();
	shader.setStrings(&preprocessed_cstr, 1);

	if (!shader.parse(&resources, 100, false, messages))
	{
		std::cout << "GLSL Parsing Failed for: " << path << std::endl;
		std::cout << shader.getInfoLog() << std::endl;
		std::cout << shader.getInfoDebugLog() << std::endl;
	}

	glslang::TProgram program;
	program.addShader(&shader);

	if(!program.link(messages))
	{
		std::cout << "GLSL Linking Failed for: " << path << std::endl;
		std::cout << shader.getInfoLog() << std::endl;
		std::cout << shader.getInfoDebugLog() << std::endl;
	}

	// if (!program.mapIO())
	// {
	// 	std::cout << "GLSL Linking (Mapping IO) Failed for: " << path << std::endl;
	// 	std::cout << Shader.getInfoLog() << std::endl;
	// 	std::cout << Shader.getInfoDebugLog() << std::endl;
	// }

	std::vector<unsigned int> spirv;
	spv::SpvBuildLogger logger;
	glslang::SpvOptions spv_options;
	glslang::GlslangToSpv(*program.getIntermediate(shader_type), spirv, &logger, &spv_options);

	// Output the resulting binary.
	//glslang::OutputSpvBin(spirv, (path + ".spv").c_str());

	if (logger.getAllMessages().length() > 0)
	{
		std::cout << logger.getAllMessages() << std::endl;
	}

	//glslang::FinalizeProcess();

	return spirv;
}

EShLanguage CompilerSPIRV::get_stage(const std::string& stage) const
{
	if (stage == "vert")
		return EShLangVertex;
	else if (stage == "tesc")
		return EShLangTessControl;
	else if (stage == "tese")
		return EShLangTessEvaluation;
	else if (stage == "geom")
		return EShLangGeometry;
	else if (stage == "frag")
		return EShLangFragment;
	else if (stage == "comp")
		return EShLangCompute;

	return EShLangCount;
}

EShLanguage CompilerSPIRV::get_stage(gfx::Stage stage) const
{
	switch (stage) {
		case gfx::Stage::Vertex:
			return EShLangVertex;
		case gfx::Stage::Fragment:
			return EShLangFragment;
	}
	return EShLangCount;
}

}}
