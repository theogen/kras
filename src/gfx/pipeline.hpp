#pragma once

#include "vertex_input.hpp"
#include "core/reader.hpp"
#include "scc/compiler_glsl.hpp"

namespace kr {
namespace gfx {

enum class IndexType { UInt16, UInt32 };

enum class CullMode { None, Front, Back, FrontAndBack };

enum class FrontFace { CW, CCW };

enum class Stage {
	Vertex,
	Fragment
};

struct Shader {
	const char* path = nullptr;
	const char* source = nullptr;
};

class Pipeline {
public:
	struct Info {
		const char* name;
		Shader vs;
		Shader fs;
		VertexInput input;
		CullMode cull_mode = CullMode::Back;
		FrontFace front_face = FrontFace::CCW;
		Reader* reader;
	};

	virtual ~Pipeline() {}

	virtual scc::Reflection get_vs_reflection() = 0;
	virtual scc::Reflection get_fs_reflection() = 0;

	virtual int get_uniform_location(const std::string& name) = 0;
};

}}
