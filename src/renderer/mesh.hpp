#pragma once

#include <vector>
#include <unordered_map>

#include "gfx/device.hpp"
#include "node/camera.hpp"
#include "res/res.hpp"

namespace kr {
namespace renderer {

class MeshRenderer {
public:
	struct Info {
		gfx::Device* device;
		res::Loader* loader;
	};

	struct Resource {
		gfx::Buffer* indices;
		gfx::Buffer* vertices;
	};

	MeshRenderer(const Info& info);

	void set_camera(node::Camera* camera);
	node::Camera* camera() { return p_camera; }

	Resource add_model(gfx::Pipeline* pipeline, const res::Model& model);

	bool has_model(gfx::Pipeline* pipeline, const std::string& name);

	gfx::Device* device() const { return p_device; }
	
private:
	struct Resources {
		std::unordered_map<std::string, Resource> models;
	};

	std::unordered_map<gfx::Pipeline*, Resources*> m_resources;

	gfx::Device* p_device;
	res::Loader* p_loader;
	node::Camera* p_camera;
};

}}
