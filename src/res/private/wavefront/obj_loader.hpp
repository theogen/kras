#pragma once

#include <string>
#include <vector>

#include "tinyobjloader/tiny_obj_loader.h"

#include "core/reader.hpp"
#include "res/loader.hpp"

namespace kr {
namespace res {

class OBJLoader {
public:
	OBJLoader(Reader* reader) : p_reader(reader) {}

	Model load(const std::string& path);

private:
	Model::Vertex create_vertex(const tinyobj::attrib_t& attrib, const tinyobj::index_t& index);

	void print_debug_info(const Model& model);

private:
	Reader* p_reader;
};

}}
