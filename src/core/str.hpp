#pragma once

#include <string>
#include <istream>
#include <vector>

namespace kr {

class Str {
public:
	/***** Case *****/

	static std::string lower(std::string s);
	static std::string lower_ascii(std::string s);
	static std::string upper(std::string s);
	static std::string upper_ascii(std::string s);

	/***** Numbers *****/

	static bool strtol(const std::string& s, long *res, int base = 10);
	static long   atoi(const std::string& s, int base = 10);
	static double atof(const std::string& s);

	static std::string itoa(int i);
	static std::string ftoa(double d);

	/***** Parsing *****/

	static bool getline(std::istream& is, std::string* str, const char* delim = "\n", bool skipempty = true);

	static void split(std::vector<std::string>* out, const std::string& str, char delim, unsigned count = 0);

	static std::string memtostr(const char *start, const char *end);
};

}
