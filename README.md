# Kras

## About

Kras is a versalite and modular game development framework designed to be as
minimal and hassle-free as possible. It provides an easy Lua API (work in
progress) and gives you the tools to make the game you want without it getting
in your way.

## Building

It's a bit difficult to compile at the moment, because I haven't developed on
other system than mine. You need SConstruct, glslang, SDL2, Vulkan headers,
GLEW and lua.

```shell
git clone --recurse-submodules https://gitlab.com/theogen/kras
# Need to compile SPIRV-Cross manually at the moment
cd kras/thirdparty/SPIRV-Cross
mkdir spirv-cross
cd spirv-cross
cmake ..
make
cd ../../../
# Compile the library and demos
scons
# Run demo game
./build/debug/bin/ship
```

## Demo game

![Screenshot](sample/ship/screenshot.jpg)

It's very basic space game where you have to dodge the rockets.
