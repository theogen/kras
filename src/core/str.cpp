#include <algorithm>
#include <errno.h>
#include <stdlib.h>
#include <cstring>

#include "str.hpp"

namespace kr {

std::string Str::lower(std::string s)
{
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	return s;
}

std::string Str::lower_ascii(std::string s)
{
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	return s;
}

std::string Str::upper(std::string s)
{
	std::transform(s.begin(), s.end(), s.begin(), ::toupper);
	return s;
}

std::string Str::upper_ascii(std::string s)
{
	std::transform(s.begin(), s.end(), s.begin(), ::toupper);
	return s;
}


bool Str::strtol(const std::string& s, long *res, int base)
{
	char *endptr;

	if (s[0] == '0') {
		base = 8;
		if (s[1] == 'x')
			base = 16;
	}

	*res = ::strtol(&s[0], &endptr, base);
	if (*endptr != 0 || s == endptr || errno == ERANGE)
		return false;

	return true;
}

long Str::atoi(const std::string& s, int base)
{
	long res;
	if (!strtol(s, &res, base))
		return 0;
	return res;
}

// https://stackoverflow.com/a/277810/9432170
static void morph_numeric_string(char *s, int n)
{
	char *p;
	int count;

	// Find decimal point, if any.
	p = strchr(s, '.');
	if (p == nullptr)
		return;

	// Adjust for more or less decimals.
	count = n;
	while (count >= 0) {
		--count;
		if (*p == '\0')
			break;
		++p;
	}

	// Truncate string.
	*p-- = '\0';

	// Remove trailing zeros.
	while (*p == '0')
		*p-- = '\0';

	// If all decimals were zeros, remove ".".
	if (*p == '.')
		*p = '\0';
}

double Str::atof(const std::string& s)
{
	return ::atof(&s[0]);
}


std::string Str::itoa(int i)
{
	return std::to_string(i);
}

std::string Str::ftoa(double d)
{
	std::string res = std::to_string(d);
	morph_numeric_string(&res[0], 3);
	res = std::string(res.c_str());
	return res;
}


bool Str::getline(std::istream& is, std::string* str, const char* delim, bool skipempty)
{
	typedef std::istreambuf_iterator<char> iterator;
	static iterator eof;
	iterator i(is);

	char c;

	// Skipping whitespaces.
	if (skipempty) {
		for (; i != eof; ++i) {
			c = *i;
			if (!strchr(delim, c))
				break;
		}
	}
	
	if (i == eof)
		return false;

	for (;i != eof; ++i) {
		c = *i;
		if (!strchr(delim, c))
			*str += c;
		else
			break;
	}

	return true;
}

void Str::split(std::vector<std::string>* out, const std::string& str, char delim, unsigned count)
{
	out->clear();
	unsigned occurences = 0;
	std::string current = "";
	for (unsigned i = 0; i < str.length(); ++i) {
		char c = str[i];
		if (c == delim) {
			out->push_back(current);
			current = "";
			if (count == ++occurences)
				return;
			continue;
		}
		current += c;
	}
	if (current.length() > 0)
		out->push_back(current);
}

std::string Str::memtostr(const char *start, const char *end)
{
	std::string s;
	size_t len = end - start;
	for (size_t i = 0; i < len; ++i)
		s += start[i];
	return s;
}

}
