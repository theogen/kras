#pragma once

#include <glslang/Public/ShaderLang.h>

#include "core/reader.hpp"
#include "gfx/pipeline.hpp"

namespace kr {
namespace scc {

class CompilerSPIRV {
public:
	CompilerSPIRV(Reader* reader);

	std::vector<uint32_t> compile(const std::string& path);
	std::vector<uint32_t> compile(const char* source, gfx::Stage stage, const std::string& path);

private:
	EShLanguage get_stage(const std::string& stage) const;
	EShLanguage get_stage(gfx::Stage stage) const;

	static bool glslang_initialized;
	Reader* p_reader;
};

}}
