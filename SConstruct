# vim: ft=python

import os
import shutil
import glob
import shutil 
import subprocess

# Program/library name.
progname = 'kras'

# SCons environment.
env = Environment()
# Copying environment variables.
for key, value in os.environ.items():
    env['ENV'][key] = value

#########################################################################
# OPTIONS ###############################################################
#########################################################################

AddOption(
    '--progname',
    dest='progname',
    nargs=1, type='string',
    action='store',
    metavar='name',
    default=progname,
    help='Build & install program by given name.'
)
progname = GetOption('progname')

AddOption(
    '--prefix',
    dest='prefix',
    nargs=1, type='string',
    action='store',
    metavar='DIR',
    default='install',
    help='Installation prefix.'
)

AddOption(
    '--build-prefix',
    dest='buildpfx',
    nargs=1, type='string',
    action='store',
    metavar='DIR',
    default='build',
    help='Build prefix.'
)

AddOption(
    '--verbose',
    dest='verbose',
    action='store_true',
    default=False,
    help='Verbose output.'
)

#########################################################################
# VERBOSITY #############################################################
#########################################################################

if not GetOption('verbose'):
    env.Append(
        CXXCOMSTR = "\033[32mCompiling `\033[39;1m$TARGET\033[32;21;24m'\033[0m",
        LINKCOMSTR = "\033[36mLinking `\033[39;1m$TARGET\033[36;21;24m'\033[0m",
        ARCOMSTR = "\033[32mArchiving `\033[39;1m$TARGET\033[32;21;24m'\033[0m",
        RANLIBCOMSTR = "\033[32mMaking library `\033[39;1m$TARGET\033[32;21;24m'\033[0m",
        INSTALLSTR = "\033[34mInstall `\033[39;1m$TARGET\033[34;21;24m'\033[0m",
        CLEANSTR = "\033[31mRemove `\033[39;1m$TARGET\033[31;21;24m'\033[0m"
    )


#########################################################################
# BUILD TYPE ############################################################
#########################################################################

# Build type.
if ARGUMENTS.get('build') != 'release':
    build_type = 'debug'
else:
    build_type = 'release'


#########################################################################
# DIRECTORIES ###########################################################
#########################################################################

src_dir = 'src'
samples_dir = 'sample'
lib_dir = 'thirdparty'

# Build prefix.
build_pfx = GetOption('buildpfx')

# Adding build type.
build_dir = build_pfx + '/' + build_type

build_bin_dir = build_dir + '/bin'
build_lib_dir = build_dir + '/lib'
build_obj_dir = build_dir + '/obj'

target_path = build_lib_dir + '/' + progname

# Installation prefix.
install_pfx = GetOption('prefix')

install_inc_dir = install_pfx + '/include/' + progname
install_lib_dir = install_pfx + '/lib'

env.VariantDir(build_obj_dir + '/' + progname, src_dir, duplicate=0)

# Including all subdirectories recursively.
def get_sources(directory):
    sources = Glob(directory + '/*.cpp')
    for dir in [x[0] for x in os.walk(src_dir)]:
        wo_basedir = dir[len(src_dir):]
        if len(wo_basedir) == 0:
            continue
        sources += Glob(directory + '/' + wo_basedir + '/*.cpp')
    return sources

sources = get_sources(build_obj_dir + '/' + progname)

#########################################################################
# FLAGS #################################################################
#########################################################################

# Show all warnings.
flags  = ' -Wall'

# Build type.
if build_type == 'debug':
    flags += ' -g -rdynamic'
else:
    flags += ' -O3 -DNDEBUG'

# C11 standard.
flags += ' -std=c11'

# Source directory.
flags += ' -I' + src_dir
flags += ' -I.'

# Libraries directory.
flags += ' -isystem' + lib_dir

# Colored output.
flags += ' -fdiagnostics-color'

flags += ' -L' + build_lib_dir
flags += ' -L thirdparty/SPIRV-Cross/spirv-cross'


#########################################################################
# LIBRARIES #############################################################
#########################################################################

# Libraries.
env.Append(LIBS = [
    'SDL2',
    'SDL2_image',
    'vulkan',
    'GL',
    'GLEW',
    'glslang',
    'SPIRV',
    'spirv-cross-core',
    'spirv-cross-cpp',
    'spirv-cross-glsl',
    'lua'
])


#########################################################################
# BUILD #################################################################
#########################################################################

# Library
lib = env.Library(target_path, sources, parse_flags = flags)

# Samples
for f in ['ship']:
    path = build_obj_dir + '/sample/' + f
    env.VariantDir(path, samples_dir + '/' + f, duplicate=0)
    sources = lib + get_sources(path)
    env.Prepend(LIBS = ['kras'])
    program = env.Program(build_bin_dir + '/' + f, sources, parse_flags = flags)


#########################################################################
# RUN ###################################################################
#########################################################################

#execution = env.Command("run", None, "./" + target_path + " testproj")
#Depends(execution, program)
#Default(program)
#env.Alias('run', execution)
