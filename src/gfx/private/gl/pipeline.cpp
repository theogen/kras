#include <cstdlib>
#include <iostream>

#include "core/filesys.hpp"

#include "scc/compiler_spirv.hpp"

#include "device.hpp"
#include "pipeline.hpp"
#include "format.hpp"
#include "image.hpp"

using namespace kr::scc;

namespace kr {
namespace gfx {
namespace gl {

Pipeline::Pipeline(const Info& info, Device* device)
	: p_device(device), p_reader(info.reader)
{
	glGenVertexArrays(1, &m_vertex_array);

	CCResult vsres = cross_compile(Stage::Vertex, info.vs);
	CCResult fsres = cross_compile(Stage::Fragment, info.fs);

	load_cc_res(vsres);
	load_cc_res(fsres);

	link_shader(vsres.reflection.input);

	if (!info.input.attributes.empty()) {
		std::cout << "Using custom provided vertex input\n\n";
		m_input = info.input;
	}
	else {
		std::cout << "Using automatic vertex input\n\n";
		size_t size = 0;
		for (const Resource& res : vsres.reflection.input) {
			m_input.attributes.push_back(
				kr::gfx::VertexInput::AttrDesc(0, res.location, res.type.format, size)
			);
			size += res.type.size;
		}

		m_input.bindings = {
			kr::gfx::VertexInput::BindingDesc(0, size, kr::gfx::VertexInput::Rate::Vertex)
		};
	}

	m_vs_reflection = vsres.reflection;
	m_fs_reflection = fsres.reflection;

	switch (info.cull_mode) {
		case CullMode::None:
			m_cull_mode = 0;
			break;
		case CullMode::Front:
			m_cull_mode = GL_FRONT;
			break;
		case CullMode::Back:
			m_cull_mode = GL_BACK;
			break;
		case CullMode::FrontAndBack:
			m_cull_mode = GL_FRONT_AND_BACK;
			break;
	};

	switch (info.front_face) {
		case FrontFace::CW:
			m_front_face = GL_CW;
			break;
		case FrontFace::CCW:
			m_front_face = GL_CCW;
			break;
	}
}

Pipeline::~Pipeline()
{
	// Check if this shader has been initialized.
	if (m_program_id != (unsigned)-1)
	{
		// Stop the shader.
		glUseProgram(0);

		// Delete individual stages.
		for (size_t i = 0; i < m_stages_id.size(); ++i) {
			if (m_stages_id[i] != (unsigned)-1) {
				glDetachShader(m_program_id, m_stages_id[i]);
				glDeleteShader(m_stages_id[i]);
			}
		}

		// Delete the program.
		glDeleteProgram(m_program_id);
	}

	glDeleteVertexArrays(1, &m_vertex_array);
}

int Pipeline::get_uniform_location(const std::string& name)
{
	for (const Resource& res : m_vs_reflection.uniform)
		if (res.name == name)
			return res.binding;
	for (const Resource& res : m_fs_reflection.uniform)
		if (res.name == name)
			return res.binding;
	for (const Resource& res : m_fs_reflection.samplers)
		if (res.name == name)
			return res.binding;
	return -1;
}

void Pipeline::bind()
{
	glBindVertexArray(m_vertex_array);
	if (m_cull_mode) {
		glEnable(GL_CULL_FACE);
		glCullFace(m_cull_mode);
		glFrontFace(m_front_face);
	}
}

void Pipeline::bind_index_buffer(gfx::Buffer* buffer, IndexType type)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dynamic_cast<Buffer*>(buffer)->id());
	m_index_type = type == IndexType::UInt16 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT;
}

void Pipeline::bind_vertex_buffer(gfx::Buffer* buffer)
{
	glBindBuffer(GL_ARRAY_BUFFER, dynamic_cast<Buffer*>(buffer)->id());
	for (uint32_t i = 0; i < m_input.bindings.size(); ++i) {
		auto binding = m_input.bindings[i];

		for (const auto& attrib : m_input.attributes)
		{
			if (attrib.binding != i)
				continue;
			FormatInfo fmt = get_format_info(attrib.format);
			glVertexAttribPointer(
				attrib.location,
				fmt.size,
				fmt.type,
				fmt.normalized,
				binding.stride,
				reinterpret_cast<void*>(attrib.offset)
			);
			glVertexAttribDivisor(
				attrib.location,
				binding.rate == VertexInput::Rate::Vertex ? 0 : 1
			);
		}
	}
}

void Pipeline::bind_descriptors(const std::unordered_map<int, Descriptor>& descriptors)
{
	for (auto const& kv : descriptors)
	{
		const Descriptor& desc = kv.second;
		if (desc.is_buffer) {
			Buffer* buffer = dynamic_cast<Buffer*>(desc.buffer);
			glBindBufferRange(
				GL_UNIFORM_BUFFER,
				kv.first,
				buffer->id(),
				0,
				desc.buffer->size()
			);
		}
		else if (desc.is_image) {
			Image* image = dynamic_cast<Image*>(desc.image);
			glActiveTexture(GL_TEXTURE0 + kv.first);
			glBindTexture(
				image->target(),
				image->id()
			);
		}
	}
}

void Pipeline::draw(size_t vertices_count)
{
	glUseProgram(m_program_id);
	for (auto attrib : m_input.attributes) {
		glEnableVertexAttribArray(attrib.location);
	}

	glDrawElements(
		GL_TRIANGLES,
		vertices_count,
		m_index_type, 0
	);

	for (auto attrib : m_input.attributes) {
		glDisableVertexAttribArray(attrib.location);
	}

	glBindVertexArray(0);

	glUseProgram(0);
}


Pipeline::CCResult Pipeline::cross_compile(Stage stage, Shader shader)
{
	CCResult res;

	// Compiling original GLSL to SPIR-V.
	CompilerSPIRV spirv(p_reader);

	std::vector<uint32_t> spirv_binary;
	if (shader.source != nullptr) {
		spirv_binary = spirv.compile(shader.source, stage, shader.path);
	}
	else if (shader.path != nullptr) {
		std::cout << "Compiling " << shader.path << " to SPIR-V" << std::endl;
		spirv_binary = spirv.compile(shader.path);
	}

	// Compiling resulting SPIR-V to selected GLSL version.
	CompilerGLSL glsl(std::move(spirv_binary));
	CompilerGLSL::Options options;
	{
		options.version = 150;
		options.es = false;
	}
	res.reflection = glsl.reflect();
	//std::cout << "Cross-compiling " << path << " to GLSL" << std::endl;
	res.source = glsl.compile(options);
	res.stage = stage;

	return res;
}


void Pipeline::load_cc_res(const CCResult& shader)
{
	load_stage(shader.source.c_str(), shader.source.length(), shader.stage);
}

/* Shaders */

void Pipeline::load_stage(const char* data, GLint size, Stage stage)
{
	GLenum type   = get_gl_stage(stage);
	GLuint shader = glCreateShader(type);

	// Compiling shader.
	glShaderSource(shader, 1, &data, &size);
	glCompileShader(shader);

	// Getting compilation status.
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	// Compilation failed.
	if (status == GL_FALSE) {
		// Storing logs in a buffer.
		GLint length;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
		std::vector<char> buffer(length);
		glGetShaderInfoLog(shader, length, nullptr, buffer.data());

		// Deleting the shader.
		glDeleteShader(shader);

		// Printing logs.
		throw std::runtime_error(buffer.data());
	}

	// If compilation went well, add this shader to the pipeline.
	m_stages_id.push_back(shader);
}

void Pipeline::link_shader(const std::vector<Resource>& input)
{
	// Creating shader.
	m_program_id = glCreateProgram();

	// Attaching shaders.
	for (GLuint stage : m_stages_id)
		glAttachShader(m_program_id, stage);

	for (const Resource& res : input)
		glBindAttribLocation(m_program_id, res.location, res.name.c_str());

	// Linking and validating program.
	glLinkProgram(m_program_id);
	glValidateProgram(m_program_id);
}


GLenum Pipeline::get_gl_stage(Stage stage)
{
	switch (stage) {
		case Stage::Vertex:
			return GL_VERTEX_SHADER;
		case Stage::Fragment:
			return GL_FRAGMENT_SHADER;
	}

	return 0;
}

}}}
