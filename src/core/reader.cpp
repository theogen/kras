#include "reader.hpp"
#include "core/filesys.hpp"

namespace kr {

bool FileReader::exists(const std::string& path) const
{
	return Filesys::file_exists(resolve(path));
}

void FileReader::open(const std::string& path)
{
	m_file.open(resolve(path), std::ios_base::binary | std::ios_base::ate);
	m_length = m_file.tellg();
}

void FileReader::read(void* data)
{
	m_file.seekg(0, m_file.beg);
	m_file.read(static_cast<char*>(data), m_length);
}

size_t FileReader::length() const
{
	return m_length;
}

void FileReader::close()
{
	m_file.close();
	m_length = 0;
}

std::string FileReader::resolve(const std::string& path) const
{
	return path;
}

}
