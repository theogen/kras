#pragma once

#include <string>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace kr {
namespace node {

class Node {
public:
	Node(const std::string& name) : m_name(name) {}


	virtual void update() = 0;


	inline std::string name() const { return m_name; }

	inline void set_name(const std::string& name) { m_name = name; }


	inline bool active() const { return m_active; }
	virtual void set_active(bool active) { m_active = active; }


	glm::mat4 transform_matrix() const
	{
		glm::mat4 transform(1);
		transform = glm::translate(transform, m_position);
		transform = glm::rotate(transform, glm::radians(m_rotation.x), glm::vec3(1, 0, 0));
		transform = glm::rotate(transform, glm::radians(m_rotation.y), glm::vec3(0, 1, 0));
		transform = glm::rotate(transform, glm::radians(m_rotation.z), glm::vec3(0, 0, 1));
		transform = glm::scale(transform, m_scale);
		return transform;
	}


	inline glm::vec3 position() const { return m_position; }

	inline void set_position(const glm::vec3& pos) { m_position = pos; }

	void translate(const glm::vec3& offset, float delta)
	{
		m_position += offset * delta;
	}


	inline glm::vec3 rotation() const { return m_rotation; }

	inline void set_rotation(const glm::vec3& rot) { m_rotation = rot; }

	void rotate(const glm::vec3& offset, float delta = 1)
	{
		m_rotation += offset * delta;
	}


	inline glm::vec3 scale() const { return m_scale; }

	inline void set_scale(const glm::vec3& scale) { m_scale = scale; }


protected:
	virtual ~Node() {}

	// Name of this node.
	std::string m_name;

	// Whether this node is active by itself.
	bool m_active = true;

	// Local position of this node.
	glm::vec3 m_position = glm::vec3(0, 0, 0);
	// Local rotation of this node.
	glm::vec3 m_rotation = glm::vec3(0, 0, 0);
	// Local scale of this node.
	glm::vec3 m_scale = glm::vec3(1, 1, 1);
};

}}
