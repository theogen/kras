#pragma once

#include "format.hpp"

namespace kr {
namespace gfx {

enum class Filter {
	Nearest,
	Linear
};

class Image {
public:
	struct Info {
		Format format;
		uint32_t width;
		uint32_t height;
		const void* pixels = nullptr;
		Filter mag_filter = Filter::Linear;
		Filter min_filter = Filter::Linear;
		bool gen_mipmap = false;
	};

	virtual ~Image() {}

	virtual uint32_t width() const noexcept = 0;
	virtual uint32_t height() const noexcept = 0;
};

}}
