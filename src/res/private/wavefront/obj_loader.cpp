#include <set>
#include <unordered_map>
#include <iostream>

#include "obj_loader.hpp"
#include "res/loader.hpp"
#include "core/filesys.hpp"

#define TINYOBJLOADER_IMPLEMENTATION
#include "tinyobjloader/tiny_obj_loader.h"

namespace kr {
namespace res {

struct StreamBuf : public std::basic_streambuf<char, std::char_traits<char>> {
	StreamBuf(std::vector<char>& vec)
	{
		setg(vec.data(), vec.data(), vec.data() + vec.size());
	}
};

Model OBJLoader::load(const std::string& path)
{
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warn, err;

	std::cout << "Loading model `" << path << "'\n\n";

	p_reader->open(path);
	std::vector<char> obj_data(p_reader->length());
	p_reader->read(obj_data.data());
	p_reader->close();
	StreamBuf obj_streambuf = StreamBuf(obj_data);
	std::istream obj_stream(&obj_streambuf);

	tinyobj::MaterialStreamReader* mat_reader = nullptr;

	std::string mat_path = Filesys::remove_suffix(path) + ".mtl";
	bool has_material = p_reader->exists(mat_path);
	if (has_material) {
		p_reader->open(mat_path);
	}

	std::vector<char> mat_data(p_reader->length());
	if (has_material) {
		p_reader->read(mat_data.data());
		p_reader->close();
		StreamBuf mat_streambuf = StreamBuf(mat_data);
		std::istream mat_stream(&mat_streambuf);
		mat_reader = new tinyobj::MaterialStreamReader(mat_stream);
	}

	bool success = tinyobj::LoadObj(
		&attrib,
		&shapes,
		&materials,
		&warn, &err,
		&obj_stream,
		mat_reader
	);

	if (!warn.empty())
		std::cerr << warn << "\n";
	if (!err.empty())
		std::cerr << err << "\n";
	if (!success)
		throw std::runtime_error("Failed to load model " + path);
	
	std::unordered_map<Model::Vertex, uint32_t> unique_vertices = {};

	Model model;
	for (const auto& shape : shapes) {
		// Loop over faces.
		size_t index_offset = 0;
		for (size_t f = 0; f < shape.mesh.num_face_vertices.size(); ++f) {
			uint8_t fv = shape.mesh.num_face_vertices[f];
			int material_id = shape.mesh.material_ids[f];
			// Loop over vertices in the face.
			for (size_t v = 0; v < fv; v++) {
				auto index = shape.mesh.indices[index_offset + v];
				auto vertex = create_vertex(attrib, index);

				// Per-face material. But it needs to be stored per-vertex.
				vertex.material = material_id;
				vertex.color = {
					materials[material_id].diffuse[0],
					materials[material_id].diffuse[1],
					materials[material_id].diffuse[2]
				};

				// Add this vertex only if it's unique.
				if (unique_vertices.count(vertex) == 0) {
					unique_vertices[vertex] = model.vertices.size();
					model.vertices.push_back(vertex);
				}

				model.indices.push_back(unique_vertices[vertex]);
			}
			index_offset += fv;
		}
	}


	if (has_material) {
		delete mat_reader;
		for (const auto& m : materials) {
			Model::Material mat;
			mat.name = m.name;
			mat.diffuse = glm::vec3(m.diffuse[0], m.diffuse[1], m.diffuse[2]);
			model.materials.push_back(mat);
		}
	}

	//print_debug_info(model);

	model.path = path;

	return model;
}

Model::Vertex OBJLoader::create_vertex(const tinyobj::attrib_t& attrib, const tinyobj::index_t& index)
{
	Model::Vertex vertex = {};

	vertex.pos = {
		attrib.vertices[3 * index.vertex_index + 0],
		attrib.vertices[3 * index.vertex_index + 1],
		attrib.vertices[3 * index.vertex_index + 2]
	};

	if (attrib.normals.size() != 0)
	{
		vertex.normal = {
			attrib.normals[3 * index.normal_index + 0],
			attrib.normals[3 * index.normal_index + 1],
			attrib.normals[3 * index.normal_index + 2]
		};
	}

	if (attrib.texcoords.size() != 0)
	{
		vertex.texcoord = {
			attrib.texcoords[2 * index.texcoord_index + 0],
			1.0 - attrib.texcoords[2 * index.texcoord_index + 1]
		};
	}

	if (attrib.colors.size() != 0)
	{
		vertex.color = {
			attrib.colors[3 * index.vertex_index + 0],
			attrib.colors[3 * index.vertex_index + 1],
			attrib.colors[3 * index.vertex_index + 2]
		};
	}

	return vertex;
}

void OBJLoader::print_debug_info(const Model& model)
{
	for (const auto& material : model.materials) {
		std::cout
			<< material.name << " "
			<< material.diffuse[0] << " "
			<< material.diffuse[1] << " "
			<< material.diffuse[2] << '\n';
	}

	std::set<int> materials_used;

	for (const Model::Vertex& vertex : model.vertices)
		if (materials_used.find(vertex.material) == materials_used.end())
			materials_used.insert(vertex.material);

	for (int id : materials_used)
		std::cout << "Uses material #" << id << '\n';
}

}}
