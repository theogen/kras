#pragma once

#include <SDL2/SDL.h>

namespace kr {

struct Time {

	Time() : 
		m_ticks(0),
		m_frame(-1),
		m_time(0),
		m_delta(0),
		m_last(0)
	{}

	inline void update() noexcept
	{
		m_ticks = SDL_GetTicks();
		m_last = m_time;
		m_time = static_cast<double>(m_ticks) / 100.0;
		m_delta = m_time - m_last;
		++m_frame;
	}

	inline double time() const noexcept { return m_time; }
	inline double delta() const noexcept { return m_delta; }
	inline unsigned ticks() const noexcept { return m_ticks; }
	inline unsigned long frame() const noexcept { return m_frame; }

private:
	unsigned m_ticks;
	unsigned long m_frame;
	double m_time;
	double m_delta;
	double m_last;
};

}
