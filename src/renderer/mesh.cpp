#include "mesh.hpp"

namespace kr {
namespace renderer {

struct Attribute {
	scc::Semantic semantic;
	int size;
	size_t offset;
};

MeshRenderer::MeshRenderer(const Info& info)
	: p_device(info.device), p_loader(info.loader)
{
}

void MeshRenderer::set_camera(node::Camera* camera)
{
	p_camera = camera;
}

MeshRenderer::Resource MeshRenderer::add_model(gfx::Pipeline* pipeline, const res::Model& model)
{
	if (has_model(pipeline, model.path))
		return m_resources[pipeline]->models[model.path];

	kr::gfx::Buffer::Info buffer;

	// Index buffer.
	{
		buffer.usage = kr::gfx::BufferUsage::Index;
		buffer.size = model.indices.size() * sizeof(model.indices[0]);
		buffer.data = model.indices.data();
	}
	gfx::Buffer* indices = p_device->create_buffer(buffer);

	std::vector<Attribute> attributes;
	size_t attrsize = 0;
	for (const auto& attr : pipeline->get_vs_reflection().input) {
		attributes.push_back({attr.semantic, attr.type.size, attrsize});
		attrsize += attr.type.size;
	}

	char* sel_vertices = new char[attrsize * model.vertices.size()];

	for (size_t i = 0; i < model.vertices.size(); ++i) {
		const res::Model::Vertex& v = model.vertices[i]; 
		for (const Attribute& attr : attributes) {
			void* ptr = sel_vertices + attrsize * i + attr.offset;
			switch (attr.semantic) {
				case scc::Semantic::Position:
					*(static_cast<glm::vec3*>(ptr)) = v.pos;
					break;
				case scc::Semantic::Normal:
					*(static_cast<glm::vec3*>(ptr)) = v.normal;
					break;
				case scc::Semantic::TexCoord:
					*(static_cast<glm::vec2*>(ptr)) = v.texcoord;
					break;
				case scc::Semantic::Color:
					*(static_cast<glm::vec3*>(ptr)) = v.color;
					break;
				case scc::Semantic::Material:
					*(static_cast<float*>(ptr)) = v.material;
					break;
				default:
					break;
			};
		}
	}

	// Vertex buffer.
	{
		buffer.usage = kr::gfx::BufferUsage::Vertex;
		buffer.size = attrsize * model.vertices.size();
		buffer.data = sel_vertices;
	}
	gfx::Buffer* vertices = p_device->create_buffer(buffer);

	delete sel_vertices;

	Resource res;
	res.indices = indices;
	res.vertices = vertices;

	if (!m_resources.count(pipeline)) {
		m_resources[pipeline] = new Resources();
	}

	m_resources[pipeline]->models[model.path] = res;

	return res;
}

bool MeshRenderer::has_model(gfx::Pipeline* pipeline, const std::string& name)
{
	if (!m_resources.count(pipeline)) {
		return false;
	}
	return m_resources[pipeline]->models.count(name);
}

}}
