#include <iostream>
#include <SDL2/SDL.h>

#include "event_collector.hpp"

namespace kr {
namespace event {

EventCollector::EventCollector()
{
	p_keyboard_state = SDL_GetKeyboardState(&m_keys_count);
	m_key_state.resize(m_keys_count);
	for (int i = 0; i < m_keys_count; ++i)
	{
		m_key_state[i] = KeyState();
	}
}

void EventCollector::poll()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type) {
			case SDL_QUIT:
				m_quit = true;
				break;
		}
	}

	// Updating keyboard events.
	uint32_t i = 0;
	for (auto& keyval : m_key_state) {
		SDL_Scancode code = static_cast<SDL_Scancode>(i);
		keyval.downevent = !keyval.state && p_keyboard_state[code];
		keyval.upevent = keyval.state && !p_keyboard_state[code];
		keyval.state = p_keyboard_state[code];
		i++;
	}
}

bool EventCollector::state(Scancode key) const
{
	return m_key_state[static_cast<int>(key)].state;
}

bool EventCollector::down(Scancode key) const
{
	return m_key_state[static_cast<int>(key)].downevent;
}

bool EventCollector::up(Scancode key) const
{
	return m_key_state[static_cast<int>(key)].upevent;
}

}}
