#pragma once

#include <string>
#include <fstream>

namespace kr {

class Reader {
public:
	virtual ~Reader() {}

	virtual bool exists(const std::string& path) const = 0;
	virtual void open(const std::string& path) = 0;
	virtual void read(void* data) = 0;
	virtual size_t length() const = 0;
	virtual void close() = 0;

	virtual std::string resolve(const std::string& path) const = 0;
};

class FileReader : public Reader {
public:
	virtual bool exists(const std::string& path) const override;
	virtual void open(const std::string& path) override;
	virtual void read(void* data) override;
	virtual size_t length() const override;
	virtual void close() override;

	virtual std::string resolve(const std::string& path) const override;

protected:
	std::ifstream m_file;
	size_t m_length = 0;
};

}
