#pragma once

#include "pipeline.hpp"
#include "image.hpp"
#include "buffer.hpp"
#include "input.hpp"

class SDL_Window;

namespace kr {
namespace gfx {

enum class Driver {
	Vulkan,
	OpenGL
};

struct WindowInfo {
	const char* title;
	int w;
	int h;
	int x;
	int y;
	bool fullscreen = false;
	bool fullscreen_desktop = false;
	bool hidden = false;
	bool borderless = false;
	bool resizable = false;
	bool minimized = false;
	bool maximized = false;
	bool input_grab = false;
	bool highdpi = false;
};

class Device {
public:
	struct Info {
		Driver driver;
		WindowInfo window;
		int msaa = 0;
		bool vsync = false;
	};

	virtual ~Device() {}

	virtual Pipeline* create_pipeline(const Pipeline::Info& info) = 0;
	virtual Buffer* create_buffer(const Buffer::Info& info) = 0;
	virtual Image* create_image(const Image::Info& info) = 0;

	virtual void add_input(Input* input) = 0;

	virtual void render() = 0;

	virtual bool vsync() const = 0;
	virtual void set_vsync(bool state) = 0;

protected:
	void init_sdl();
	SDL_Window* create_window(const WindowInfo& info, int flags);
};

}}
