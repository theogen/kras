#pragma once

#include <vector>
#include <unordered_map>

#include "gfx/pipeline.hpp"
#include "scc/compiler_glsl.hpp"

#include "opengl.h"
#include "buffer.hpp"

#include "../../input.hpp"

namespace kr {
namespace gfx {
namespace gl {

class Device;

class Pipeline : public gfx::Pipeline {
private:
	struct CCResult {
		std::string source;
		scc::Reflection reflection;
		Stage stage;
	};

public:
	Pipeline(const Info& info, Device* device);
	~Pipeline();

	virtual scc::Reflection get_vs_reflection() override { return m_vs_reflection; }
	virtual scc::Reflection get_fs_reflection() override { return m_fs_reflection; }

	virtual int get_uniform_location(const std::string& name) override;

	void bind();
	void bind_index_buffer(gfx::Buffer* buffer, IndexType type);
	void bind_vertex_buffer(gfx::Buffer* buffer);
	void bind_descriptors(const std::unordered_map<int, Descriptor>& descriptors);
	void draw(size_t vertices_count);

private:
	CCResult cross_compile(Stage stage, Shader shader);
	void load_cc_res(const CCResult& shader);
	void load_stage(const char* data, GLint size, Stage stage);
	void link_shader(const std::vector<scc::Resource>& input);
	GLenum get_gl_stage(Stage stage);

private:
	Device* p_device;
	Reader* p_reader;

	unsigned m_vertex_array;
	GLenum m_index_type;
	VertexInput m_input;

	scc::Reflection m_vs_reflection;
	scc::Reflection m_fs_reflection;

	// Shaders.
	std::vector<GLuint> m_stages_id;
	GLuint m_program_id = -1;

	GLenum m_cull_mode;
	GLenum m_front_face;
};

}}}
