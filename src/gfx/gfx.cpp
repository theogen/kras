#include "gfx.hpp"

#include "private/gl/device.hpp"

namespace kr {
namespace gfx {

Device* create_device(const Device::Info& info)
{
	switch (info.driver) {
		case Driver::Vulkan:
			return nullptr;
		case Driver::OpenGL:
			return new gl::Device(info);
	}

	return nullptr;
}

}}
