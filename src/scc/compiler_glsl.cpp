#include <iostream>
#include <algorithm> // sort
#include "compiler_glsl.hpp"

namespace kr {
namespace scc {

struct TypeMapping {
	spirv_cross::SPIRType::BaseType base_type;
	uint32_t vecsize;
	uint32_t columns;
	const char* name;
	gfx::Format format;
};

static const TypeMapping k_types_map[] = {
	{ spirv_cross::SPIRType::Int,   1, 1, "int",	gfx::Format::R32SI },
	{ spirv_cross::SPIRType::Int,   2, 1, "int2",   gfx::Format::RG32SI },
	{ spirv_cross::SPIRType::Int,   3, 1, "int3",   gfx::Format::RGB32SI },
	{ spirv_cross::SPIRType::Int,   4, 1, "int4",   gfx::Format::RGBA32SI },
	{ spirv_cross::SPIRType::Float, 1, 1, "float",  gfx::Format::R32SF },
	{ spirv_cross::SPIRType::Float, 2, 1, "float2", gfx::Format::RG32SF },
	{ spirv_cross::SPIRType::Float, 3, 1, "float3", gfx::Format::RGB32SF },
	{ spirv_cross::SPIRType::Float, 4, 1, "float4", gfx::Format::RGBA32SF },
	{ spirv_cross::SPIRType::Half,  4, 1, "float",  gfx::Format::R16SF },
	{ spirv_cross::SPIRType::Half,  4, 2, "float2", gfx::Format::RG16SF },
	{ spirv_cross::SPIRType::Half,  4, 3, "float3", gfx::Format::RGB16SF },
	{ spirv_cross::SPIRType::Half,  4, 4, "float4", gfx::Format::RGBA16SF },
	{ spirv_cross::SPIRType::Float, 3, 4, "mat3x4", gfx::Format::Unknown },
	{ spirv_cross::SPIRType::Float, 3, 4, "mat4x3", gfx::Format::Unknown },
	{ spirv_cross::SPIRType::Float, 3, 3, "mat3x3", gfx::Format::Unknown },
	{ spirv_cross::SPIRType::Float, 4, 4, "mat4",   gfx::Format::Unknown },
};

CompilerGLSL::CompilerGLSL(std::vector<uint32_t> spirv)
	: m_spirv(spirv)
{
	// Parsing SPIR-V.
	p_glsl = new spirv_cross::CompilerGLSL(m_spirv);
}

CompilerGLSL::~CompilerGLSL()
{
	delete p_glsl;
}

Type CompilerGLSL::get_type(const spirv_cross::SPIRType& type)
{
	Type res;
	for (TypeMapping map : k_types_map) {
		if (map.base_type == type.basetype && map.vecsize == type.vecsize && map.columns == type.columns) {
			res.name = map.name;
			res.size = 4 * map.vecsize * map.columns;
			res.format = map.format;
		}
	}
	res.name = "unknown";
	return res;
}

void CompilerGLSL::reflect_resource(
		const spirv_cross::SmallVector<spirv_cross::Resource>& ress,
		std::vector<Resource>& reflection,
		ResourceType restype)
{
	if (ress.empty())
		return;

	for (const spirv_cross::Resource& res : ress) {
		const spirv_cross::SPIRType& type = p_glsl->get_type(res.type_id);

		Resource r;
		r.name = res.name;
		r.id = (int)res.id;
		r.type = get_type(type);

		spirv_cross::Bitset mask = p_glsl->get_decoration_bitset(res.id);

		int loc = -1;
		if (mask.get(spv::DecorationLocation)) {
			loc = p_glsl->get_decoration(res.id, spv::DecorationLocation);
			r.location = loc;
			r.semantic = static_cast<Semantic>(loc);
		}
		if (mask.get(spv::DecorationDescriptorSet)) {
			r.set = p_glsl->get_decoration(res.id, spv::DecorationDescriptorSet);
		}
		if (mask.get(spv::DecorationBinding)) {
			r.binding = p_glsl->get_decoration(res.id, spv::DecorationBinding);
		}
		if (mask.get(spv::DecorationInputAttachmentIndex)) {
			r.attachment = p_glsl->get_decoration(res.id, spv::DecorationInputAttachmentIndex);
		}
		if (mask.get(spv::DecorationNonWritable)) {
			r.readonly = true;
		}
		if (mask.get(spv::DecorationNonReadable)) {
			r.writeonly = true;
		}

		// Members.
		if (restype == ResourceType::UniformBuffer) {
			int member_idx = 0;
			for (auto& member_id : type.member_types) {
				Resource member;
				auto& member_type = p_glsl->get_type(member_id);

				member.name = p_glsl->get_member_name(type.self, member_idx);
				member.type = get_type(member_type);
				member.offset = p_glsl->type_struct_member_offset(type, member_idx);
				member.size = (int)p_glsl->get_declared_struct_member_size(type, member_idx);
				if (!member_type.array.empty()) {
					int count = 0;
					for (auto array : member_type.array)
						count += array;
					member.count = count;
				}
				r.members.push_back(member);

				member_idx++;
			}
		}

		reflection.push_back(r);
	}
}

Reflection CompilerGLSL::reflect()
{
	// The SPIR-V is now parsed, and we can perform reflection on it.
	spirv_cross::ShaderResources ress = p_glsl->get_shader_resources();
	Reflection reflection;

	reflect_resource(ress.uniform_buffers, reflection.uniform, ResourceType::UniformBuffer);
	//reflect_resource(ress.storage_buffers);
	reflect_resource(ress.stage_inputs, reflection.input, ResourceType::StageInput);
	std::sort(reflection.input.begin(), reflection.input.end(),
		[](const Resource& a, const Resource& b) {
			return a.location < b.location;
		}
	);
	//reflect_resource(ress.stage_outputs);
	//reflect_resource(ress.subpass_inputs);
	//reflect_resource(ress.storage_images);
	reflect_resource(ress.sampled_images, reflection.samplers, ResourceType::SampledImage);
	//reflect_resource(ress.atomic_counters);
	//reflect_resource(ress.acceleration_structures);
	//reflect_resource(ress.push_constant_buffers);
	//reflect_resource(ress.separate_images);
	//reflect_resource(ress.separate_samplers);

	return reflection;
}

std::string CompilerGLSL::compile(const Options& options)
{
	// Setting options.
	p_glsl->set_common_options(options);

	// Compile to GLSL, ready to give to GL driver.
	std::string source = p_glsl->compile();

	return source;
}

}}
