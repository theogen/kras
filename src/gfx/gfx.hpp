#pragma once

#include <cstddef>
#include <cstdint>

#include "device.hpp"

namespace kr {
namespace gfx {

Device* create_device(const Device::Info& info);

}}
