#pragma once

#include "node.hpp"

namespace kr {
namespace node {

class Camera : public Node {
public:
	enum Projection { Perspective, Ortho };

	Camera(const std::string& name) : Node(name)
	{
		recalculate_projection_matrix();
	}

	Camera(const std::string& name, Projection proj, float fov, float aspect, float near, float far) :
		Node(name),
		m_projection_type(proj),
		m_fov(fov),
		m_aspect(aspect),
		m_near(near),
		m_far(far)
	{
		recalculate_projection_matrix();
	}

	virtual void update() override {}

	glm::mat4 projection_matrix() const { return m_projection_matrix; }
	glm::mat4 view_matrix() const
	{
		glm::mat4 view(1);
		view = glm::rotate(view, glm::radians(m_rotation.x), glm::vec3(1, 0, 0));
		view = glm::rotate(view, glm::radians(m_rotation.y), glm::vec3(0, 1, 0));
		view = glm::rotate(view, glm::radians(m_rotation.z), glm::vec3(0, 0, 1));
		// Negate X and Y but leave Z as-is.
		glm::vec3 t = glm::vec3(-m_position.x, -m_position.y, m_position.z);
		view = glm::translate(view, t);
		return view;
	}

	void set_projection(Projection projection)
	{
		m_projection_type = projection;
	}

	float fov() const { return m_fov; }
	void set_fov(float fov) { m_fov = fov; }

	float aspect() const { return m_aspect; }
	void set_aspect(float aspect) { m_aspect = aspect; }

	float near() const { return m_near; }
	void set_near(float near) { m_near = near; };
	float far() const { return m_far; }
	void set_far(float far) { m_far = far; };

	float size() const { return m_size; }
	void set_size(float size) { m_size = size; }

	bool negate_projection_y() const noexcept { return m_negate_projection_y; }
	void set_negate_projection_y(bool state) noexcept
	{ m_negate_projection_y = state; }

	// Needs to be called every time changes were made.
	void recalculate_projection_matrix()
	{
		if (m_projection_type == Projection::Perspective) {
			m_projection_matrix = glm::perspective(
				glm::radians(m_fov), m_aspect, m_near, m_far
			);
		}
		else {
			float v = m_size * m_aspect / 2;
			float h = m_size / 2;

			m_projection_matrix = glm::ortho(
				v, v, h, h, m_near, m_far
			);
		}

		if (m_negate_projection_y)
			m_projection_matrix[1][1] *= -1;
	}

private:
	Projection m_projection_type = Projection::Perspective;
	glm::mat4 m_projection_matrix;

	// Field of view
	float m_fov = 70;
	// Aspect ratio
	float m_aspect = 4.0/3.0;
	// Clipping planes
	float m_near = 0.003, m_far = 1000;
	// Orthographic size
	float m_size = 1;

	bool m_negate_projection_y = false;

	float m_orthographic_size;
};

}}
