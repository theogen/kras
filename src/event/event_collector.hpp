#pragma once

#include <vector>

#include "scancode.hpp"

namespace kr {
namespace event {

class EventCollector {
public:
	struct KeyState {
		bool state, upevent, downevent;

		KeyState() : state(false), upevent(false), downevent(false) {}
	};

	EventCollector();

	void poll();
	bool is_close_requested() const { return m_quit; }

	bool state(Scancode key) const;
	bool down(Scancode key) const;
	bool up(Scancode key) const;

private:
	bool m_quit = false;
	int m_keys_count;
	const uint8_t* p_keyboard_state;
	std::vector<KeyState> m_key_state;
};

}}
