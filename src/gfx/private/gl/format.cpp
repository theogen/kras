
#include "format.hpp"

namespace kr {
namespace gfx {
namespace gl {

FormatInfo get_format_info(Format format)
{
	switch (format) {
		/* R */

		case Format::R8UN:
			return FormatInfo(GL_RED, GL_R8, 1, GL_UNSIGNED_BYTE, true);
		case Format::R8SN:
			return FormatInfo(GL_RED, GL_R8_SNORM, 1, GL_BYTE, true);
		case Format::R8SI:
			return FormatInfo(GL_RED, GL_R8I, 1, GL_BYTE, false);
		case Format::R8UI:
			return FormatInfo(GL_RED, GL_R8UI, 1, GL_UNSIGNED_BYTE, false);

		case Format::R16UN:
			return FormatInfo(GL_RED, GL_R16, 1, GL_UNSIGNED_SHORT, true);
		case Format::R16SN:
			return FormatInfo(GL_RED, GL_R16_SNORM, 1, GL_SHORT, true);
		case Format::R16SI:
			return FormatInfo(GL_RED, GL_R16I, 1, GL_SHORT, false);
		case Format::R16UI:
			return FormatInfo(GL_RED, GL_R16UI, 1, GL_UNSIGNED_SHORT, false);
		case Format::R16SF:
			return FormatInfo(GL_RED, GL_R16F, 1, GL_HALF_FLOAT, false);

		case Format::R32SI:
			return FormatInfo(GL_RED, GL_R32I, 1, GL_INT, false);
		case Format::R32UI:
			return FormatInfo(GL_RED, GL_R32UI, 1, GL_UNSIGNED_INT, false);
		case Format::R32SF:
			return FormatInfo(GL_RED, GL_R32F, 1, GL_FLOAT, false);

		/* RG */

		case Format::RG8UN:
			return FormatInfo(GL_RG, GL_RG8, 2, GL_UNSIGNED_BYTE, true);
		case Format::RG8SN:
			return FormatInfo(GL_RG, GL_RG8_SNORM, 2, GL_BYTE, true);
		case Format::RG8SI:
			return FormatInfo(GL_RG, GL_RG8I, 2, GL_BYTE, false);
		case Format::RG8UI:
			return FormatInfo(GL_RG, GL_RG8UI, 2, GL_UNSIGNED_BYTE, false);

		case Format::RG16UN:
			return FormatInfo(GL_RG, GL_RG16, 2, GL_UNSIGNED_SHORT, true);
		case Format::RG16SN:
			return FormatInfo(GL_RG, GL_RG16_SNORM, 2, GL_SHORT, true);
		case Format::RG16SI:
			return FormatInfo(GL_RG, GL_RG16I, 2, GL_SHORT, false);
		case Format::RG16UI:
			return FormatInfo(GL_RG, GL_RG16UI, 2, GL_UNSIGNED_SHORT, false);
		case Format::RG16SF:
			return FormatInfo(GL_RG, GL_RG16F, 2, GL_HALF_FLOAT, false);

		case Format::RG32SI:
			return FormatInfo(GL_RG, GL_RG32I, 2, GL_INT, false);
		case Format::RG32UI:
			return FormatInfo(GL_RG, GL_RG32UI, 2, GL_UNSIGNED_INT, false);
		case Format::RG32SF:
			return FormatInfo(GL_RG, GL_RG32F, 2, GL_FLOAT, false);

		/* RGB */

		case Format::RGB8UN:
			return FormatInfo(GL_RGB, GL_RGB8, 3, GL_UNSIGNED_BYTE, true);
		case Format::RGB8SN:
			return FormatInfo(GL_RGB, GL_RGB8_SNORM, 3, GL_BYTE, true);
		case Format::RGB8SI:
			return FormatInfo(GL_RGB, GL_RGB8I, 3, GL_BYTE, false);
		case Format::RGB8UI:
			return FormatInfo(GL_RGB, GL_RGB8UI, 3, GL_UNSIGNED_BYTE, false);

		case Format::RGB16UN:
			return FormatInfo(GL_RGB, GL_RGB16, 3, GL_UNSIGNED_SHORT, true);
		case Format::RGB16SN:
			return FormatInfo(GL_RGB, GL_RGB16_SNORM, 3, GL_SHORT, true);
		case Format::RGB16SI:
			return FormatInfo(GL_RGB, GL_RGB16I, 3, GL_SHORT, false);
		case Format::RGB16UI:
			return FormatInfo(GL_RGB, GL_RGB16UI, 3, GL_UNSIGNED_SHORT, false);
		case Format::RGB16SF:
			return FormatInfo(GL_RGB, GL_RGB16F, 3, GL_HALF_FLOAT, false);

		case Format::RGB32SI:
			return FormatInfo(GL_RGB, GL_RGB32I, 3, GL_INT, false);
		case Format::RGB32UI:
			return FormatInfo(GL_RGB, GL_RGB32UI, 3, GL_UNSIGNED_INT, false);
		case Format::RGB32SF:
			return FormatInfo(GL_RGB, GL_RGB32F, 3, GL_FLOAT, false);

		/* RGBA */

		case Format::RGBA8UN:
			return FormatInfo(GL_RGBA, GL_RGBA8, 4, GL_UNSIGNED_BYTE, true);
		case Format::RGBA8SN:
			return FormatInfo(GL_RGBA, GL_RGBA8_SNORM, 4, GL_BYTE, true);
		case Format::RGBA8SI:
			return FormatInfo(GL_RGBA, GL_RGBA8I, 4, GL_BYTE, false);
		case Format::RGBA8UI:
			return FormatInfo(GL_RGBA, GL_RGBA8UI, 4, GL_UNSIGNED_BYTE, false);

		case Format::RGBA16UN:
			return FormatInfo(GL_RGBA, GL_RGBA16, 4, GL_UNSIGNED_SHORT, true);
		case Format::RGBA16SN:
			return FormatInfo(GL_RGBA, GL_RGBA16_SNORM, 4, GL_SHORT, true);
		case Format::RGBA16SI:
			return FormatInfo(GL_RGBA, GL_RGBA16I, 4, GL_SHORT, false);
		case Format::RGBA16UI:
			return FormatInfo(GL_RGBA, GL_RGBA16UI, 4, GL_UNSIGNED_SHORT, false);
		case Format::RGBA16SF:
			return FormatInfo(GL_RGBA, GL_RGBA16F, 4, GL_HALF_FLOAT, false);

		case Format::RGBA32SI:
			return FormatInfo(GL_RGBA, GL_RGBA32I, 4, GL_INT, false);
		case Format::RGBA32UI:
			return FormatInfo(GL_RGBA, GL_RGBA32UI, 4, GL_UNSIGNED_INT, false);
		case Format::RGBA32SF:
			return FormatInfo(GL_RGBA, GL_RGBA32F, 4, GL_FLOAT, false);

		/* BGR */

		case Format::BGR8UN:
			return FormatInfo(GL_BGR, GL_RGB8, 3, GL_UNSIGNED_BYTE, true);
		case Format::BGR8SN:
			return FormatInfo(GL_BGR, GL_RGB8_SNORM, 3, GL_BYTE, true);
		case Format::BGR8UI:
			return FormatInfo(GL_BGR, GL_RGB8UI, 3, GL_UNSIGNED_BYTE, false);
		case Format::BGR8SI:
			return FormatInfo(GL_BGR, GL_RGB8I, 3, GL_BYTE, false);

		/* BGRA */

		case Format::BGRA8UN:
			return FormatInfo(GL_BGRA, GL_RGBA8, 4, GL_UNSIGNED_BYTE, true);
		case Format::BGRA8SN:
			return FormatInfo(GL_BGRA, GL_RGBA8_SNORM, 4, GL_BYTE, true);
		case Format::BGRA8UI:
			return FormatInfo(GL_BGRA, GL_RGBA8UI, 4, GL_UNSIGNED_BYTE, false);
		case Format::BGRA8SI:
			return FormatInfo(GL_BGRA, GL_RGBA8I, 4, GL_BYTE, false);

		/* Depth */

		case Format::D16:
			return FormatInfo(0, GL_DEPTH_COMPONENT16, 0, 0, false);
		case Format::D24S8:
			return FormatInfo(0, GL_DEPTH_COMPONENT24, 0, 0, false);
		case Format::D32S8:
			return FormatInfo(0, GL_DEPTH_COMPONENT32, 0, 0, false);
		case Format::D32SF:
			return FormatInfo(0, GL_DEPTH_COMPONENT32F, 0, 0, false);

		case Format::Unknown:
			return FormatInfo();
	}

	return FormatInfo();
}

}}}
