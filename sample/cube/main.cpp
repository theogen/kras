#include <kras/kras.hpp>

class App {
public:
	App() {}

	void init()
	{
		kr::gfx::Window::Info window;
		{
			window.title = "Cube";
			window.x = kr::gfx::WindowPos::Centered;
			window.y = kr::gfx::WindowPos::Centered;
			window.w = 1280;
			window.h = 800;
			window.flags = kr::gfx::WindowFlags::Resizable;
			window.driver = kr::gfx::Driver::OpenGL;
		}
		m_window = kr::gfx::Window::create(window);

		kr::Instance::Info instance;
		{
			instance.name = "Cube";
			instance.version = kr::Version(0, 1, 0);
			instance.window = m_window;
		}
		I = kr::Instance::create(instance);

		init_pipeline();

		kr::node::Camera::Info camera;
		{
			camera.aspect = 16.0 / 9.0;
			camera.fov = 90;
			camera.near = 0.1;
			camera.far = 100;
			camera.position = kr::Vector3(0, 0, -10);
		}
		m_camera = kr::node::Camera::create(I, camera);

		kr::node::Mesh::Info cube;
		{
			cube.name = "cube";
			cube.model = kr::resource::load(I, "cube.obj");
			cube.pipeline = kr::gfx::Pipeline::get(I, "Cube");
		}
		m_cube = kr::node::Mesh::create(I, cube);
		m_cube.set_position(0, 0, 10);

		m_event = kr::event::Collector::create(I);
	}

	void init_pipeline()
	{
		kr::gfx::Pipeline::Info info;

		info.name = "Cube";

		info.shaders.vertex = "cube.vert";
		info.shaders.fragment = "cube.frag";

		info.input = {
			{ kr::gfx::Attribute::Position, kr::gfx::Format::Float3 },
			{ kr::gfx::Attribute::Normal,   kr::gfx::Format::Float3 },
			{ kr::gfx::Attribute::Material, kr::gfx::Format::Float }
		};

		info.layout = {
			{ kr::gfx::Uniform::ModelViewProject },
		};

		m_pipeline = kr::gfx::Pipeline::create(I, info);
	}

	void run()
	{
		while (!m_event.is_close_requested()) {
			m_event.poll();

			m_cube.rotate(0, m_time.delta() * 10, 0);

			m_time.update();
		}
	}

private:
	kr::Window m_window;
	kr::Instance I;
	kr::gfx::Pipeline m_pipeline;
	kr::event::Collector m_event;
	kr::Time m_time;
	kr::node::Node m_root;
	kr::node::Camera m_camera;
	kr::node::Mesh m_cube;
};

void main(int argc, const char** argv)
{
	App app;

	app.init();
	app.run();

	return 0;
}
