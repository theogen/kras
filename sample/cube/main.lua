-- Title: Cube
-- Description: Draws 3D cube on the screen.

cube = kr.node.Mesh {
	name = "cube",
	model = "cube.obj",
}

function kr.update()
	cube:rotate(0, 1 * kr.time.delta, 0)
end
