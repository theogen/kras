#include <iostream>
#include "mesh.hpp"

namespace kr {
namespace node {

Mesh::Mesh(const std::string& name, renderer::MeshRenderer* renderer, const Info& info)
	: Node(name), p_renderer(renderer)
{
	p_device = renderer->device();
	p_input = new gfx::Input();
	p_device->add_input(p_input);

	m_model = info.model;
	p_pipeline = info.pipeline;

	initialize();
}

Mesh::~Mesh()
{
	delete p_input;
}

void Mesh::update()
{
	if (!m_active)
		return;
	glm::mat4 model = transform_matrix();
	glm::mat4 view = p_renderer->camera()->view_matrix();
	glm::mat4 proj = p_renderer->camera()->projection_matrix();
	glm::mat4 mvp = proj * view * model;
	if (m_transform_ubo_id != -1) {
		if (m_mvp_offset != -1)
			uniform(m_transform_ubo_id, &mvp, sizeof(mvp), m_mvp_offset);
		if (m_model_offset != -1)
			uniform(m_transform_ubo_id, &model, sizeof(model), m_model_offset);
	}
	if (m_camera_ubo_id != -1) {
		if (m_view_offset != -1)
			uniform(m_camera_ubo_id, &view, sizeof(view), m_view_offset);
		if (m_proj_offset != -1)
			uniform(m_camera_ubo_id, &proj, sizeof(proj), m_proj_offset);
	}
}

void Mesh::uniform(int index, void* data, size_t size, size_t offset)
{
	p_input->descriptors[index].buffer->write(data, size, offset);
}

void Mesh::uniform(const std::string& name, void* data, size_t size, size_t offset)
{
	int loc = p_input->pipeline->get_uniform_location(name);
	uniform(loc, data, size, offset);
}

void Mesh::uniform(int index, gfx::Image* image)
{
	p_input->descriptors[index].image = image;
}

void Mesh::uniform(const std::string& name, gfx::Image* image)
{
	int loc = p_input->pipeline->get_uniform_location(name);
	uniform(loc, image);
}

void Mesh::set_active(bool active)
{
	Node::set_active(active);
	p_input->active = active;
}

void Mesh::initialize()
{
	auto res = p_renderer->add_model(p_pipeline, m_model);
	p_input->pipeline = p_pipeline;
	p_input->indices = res.indices;
	p_input->vertices = res.vertices;
	
	auto ubos = p_pipeline->get_vs_reflection().uniform;
	auto fs_ubos = p_pipeline->get_fs_reflection().uniform;
	ubos.insert(ubos.end(), fs_ubos.begin(), fs_ubos.end());

	for (const auto& ubo : ubos) {
		bool transform = ubo.name == "Transform";
		bool camera = ubo.name == "Camera";
		if (transform)
			m_transform_ubo_id = ubo.binding;
		if (camera)
			m_camera_ubo_id = ubo.binding;

		size_t size = 0;
		for (const auto& member : ubo.members) {
			if (transform) {
				if (member.name == "mvp")
					m_mvp_offset = size;
				if (member.name == "model")
					m_model_offset = size;
			}
			if (camera) {
				if (member.name == "view")
					m_view_offset = size;
				if (member.name == "projection" || member.name == "proj")
					m_proj_offset = size;
			}
			size += member.size;
		}

		kr::gfx::Buffer::Info info;
		{
			info.usage = kr::gfx::BufferUsage::Uniform;
			info.size = size;
		}

		gfx::Buffer* buffer = p_device->create_buffer(info);
		p_input->descriptors[ubo.binding] = gfx::Descriptor(buffer);
	}

	gfx::Descriptor image;
	image.is_image = true;
	for (const auto& sampler : p_pipeline->get_fs_reflection().samplers) {
		p_input->descriptors[sampler.binding] = image;
	}
}

}}
