#include "buffer.hpp"

namespace kr {
namespace gfx {
namespace gl {

Buffer::Buffer(const Info& info)
	: m_size(info.size)
{
	GLenum usage;
	switch(info.usage) {
		case BufferUsage::Index:
			m_target = GL_ELEMENT_ARRAY_BUFFER;
			usage = GL_STATIC_DRAW;
			break;
		case BufferUsage::Vertex:
			m_target = GL_ARRAY_BUFFER;
			usage = GL_STATIC_DRAW;
			break;
		case BufferUsage::Uniform:
			m_target = GL_UNIFORM_BUFFER;
			usage = GL_DYNAMIC_DRAW;
			break;
	}

	glGenBuffers(1, &m_buffer);
	glBindBuffer(m_target, m_buffer);
	glBufferData(
		m_target,
		info.size,
		info.data,
		usage
	);
	glBindBuffer(m_target, 0);
}

Buffer::~Buffer()
{
	glDeleteBuffers(1, &m_buffer);
}

void Buffer::write(
	void* data,
	size_t size,
	size_t offset)
{
	if (size == 0)
		size = m_size;
	glBindBuffer(m_target, m_buffer);
	glBufferSubData(
		m_target,
		offset,
		size,
		data
	);
	glBindBuffer(m_target, 0);
}

}}}
