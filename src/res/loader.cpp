#include <iostream>
#include <fstream>
#include <stdexcept>

#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_STDIO
#include <stb/stb_image.h>

#include <lua.hpp>

#include "loader.hpp"
#include "core/filesys.hpp"
#include "core/str.hpp"
#include "private/wavefront/obj_loader.hpp"

namespace kr {
namespace res {

bool Model::Vertex::operator==(const Vertex& other) const
{
	return
		pos == other.pos &&
		normal == other.normal &&
		texcoord == other.texcoord &&
		color == other.color &&
		material == other.material;
}

Loader::Loader(Reader* reader, gfx::Device* device)
	: p_reader(reader), p_device(device)
{
	p_obj_loader = new OBJLoader(reader);
}

Loader::~Loader()
{
	delete p_obj_loader;
}

gfx::Image* Loader::image(const std::string& path)
{
	if (m_images.count(path))
		return m_images[path];

	p_reader->open(path);
	std::vector<stbi_uc> data(p_reader->length());
	p_reader->read(data.data());
	p_reader->close();

	int width, height, channels;

	stbi_uc* pixels = stbi_load_from_memory(
		data.data(),
		data.size(),
		&width, &height,
		&channels,
		STBI_rgb_alpha
	);

	if (!pixels)
		throw std::runtime_error(path + ": failed to load the image");

	auto mag_filter = kr::gfx::Filter::Linear;
	auto min_filter = kr::gfx::Filter::Linear;
	bool gen_mipmap = true;

	if (p_reader->exists(path + ".import")) {
		lua_State* L = lua_open(path + ".import");

		auto resolve_filter = [path](const char* str) -> kr::gfx::Filter {
			std::string filter = Str::lower(str);
			if (filter == "linear")
				return kr::gfx::Filter::Linear;
			if (filter == "nearest")
				return kr::gfx::Filter::Nearest;
			else
				throw std::runtime_error(path + ": unknown filter type; expected one of the following: linear, nearest");
		};

		lua_getglobal(L, "mag_filter");
		const char* c_mag_filter = lua_tostring(L, -1);
		if (c_mag_filter)
			mag_filter = resolve_filter(c_mag_filter);


		lua_getglobal(L, "min_filter");
		const char* c_min_filter = lua_tostring(L, -1);
		if (c_min_filter)
			min_filter = resolve_filter(c_min_filter);

		lua_getglobal(L, "mipmap");
		gen_mipmap = lua_toboolean(L, -1);

		lua_close(L);
	}

	gfx::Image::Info info;
	{
		info.format     = gfx::Format::RGBA8UN;
		info.width      = width;
		info.height     = height;
		info.pixels     = pixels;
		info.mag_filter = mag_filter;
		info.min_filter = min_filter;
		info.gen_mipmap = gen_mipmap;
	}
	gfx::Image* image = p_device->create_image(info);

	stbi_image_free(pixels);

	m_images[path] = image;

	return image;
}

Model Loader::model(const std::string& path)
{
	return p_obj_loader->load(path);
}

gfx::Pipeline* Loader::pipeline(const std::string& path)
{
	if (m_pipelines.count(path))
		return m_pipelines[path];

	lua_State* L = lua_open(path);

	gfx::Pipeline::Info info;

	lua_getglobal(L, "name");
	info.name = lua_tostring(L, -1);
	if (!info.name)
		throw std::runtime_error(path + ": pipeline name should be a string");

	lua_getglobal(L, "vs");
	std::string vspath = path + "/vs";
	info.vs.path = vspath.c_str();
	info.vs.source = lua_tostring(L, -1);
	if (!info.vs.source)
		throw std::runtime_error(path + ": vertex source should be a string");

	lua_getglobal(L, "fs");
	std::string fspath = path + "/fs";
	info.fs.path = fspath.c_str();
	info.fs.source = lua_tostring(L, -1);
	if (!info.fs.source)
		throw std::runtime_error(path + ": fragment source should be a string");

	lua_getglobal(L, "cull_mode");
	const char* c_cull_mode = lua_tostring(L, -1);
	if (c_cull_mode) {
		std::string cull_mode = c_cull_mode;
		cull_mode = Str::lower(cull_mode);
		if (cull_mode == "none")
			info.cull_mode = gfx::CullMode::None;
		else if (cull_mode == "front")
			info.cull_mode = gfx::CullMode::Front;
		else if (cull_mode == "back")
			info.cull_mode = gfx::CullMode::Back;
		else if (cull_mode == "front_and_back" || cull_mode == "frontandback" || cull_mode == "front and back")
			info.cull_mode = gfx::CullMode::FrontAndBack;
		else
			throw std::runtime_error(path + ": unknown cull mode specified; expected one of the following: none, front, back, front_and_back");
	}

	lua_getglobal(L, "front_face");
	const char* c_front_face = lua_tostring(L, -1);
	if (c_front_face) {
		std::string front_face = c_front_face;
		front_face = Str::lower(front_face);
		if (front_face == "cw")
			info.front_face = gfx::FrontFace::CW;
		else if (front_face == "ccw")
			info.front_face = gfx::FrontFace::CCW;
		else
			throw std::runtime_error(path + ": unknown front face specified; expected one of the following: cw, ccw");
	}

	info.reader = p_reader;

	gfx::Pipeline* pipeline = p_device->create_pipeline(info);

	lua_close(L);

	m_pipelines[path] = pipeline;

	return pipeline;
}

lua_State* Loader::lua_open(const std::string& path)
{
	p_reader->open(path);
	std::vector<char> data(p_reader->length());
	p_reader->read(data.data());
	p_reader->close();

	lua_State* L = luaL_newstate();
	if (!L) {
		throw std::runtime_error("Couldn't create Lua state");
	}

	int error = luaL_loadbuffer(L, data.data(), data.size(), path.c_str()) || lua_pcall(L, 0, 0, 0);
	if (error) {
		auto except = std::runtime_error(lua_tostring(L, -1));
		lua_pop(L, 1); // Pop error message from the stack
		throw except;
	}

	return L;
}

ProjectFileReader::ProjectFileReader(const std::string project_path)
	: m_project_path(project_path)
{
}

std::string ProjectFileReader::resolve(const std::string& path) const
{
	static const std::string prefix = "res://";
	std::string r = path;
	if (!path.compare(0, prefix.size(), prefix))
		r = path.substr(prefix.size());

	return m_project_path + "/" + r;
}

}}
