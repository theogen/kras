#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>

namespace kr {

class CLI {
public:
	CLI(int argc, const char** argv, const std::vector<std::string>& options);

	int arguments_count() const;

	std::string option(const std::string& key);
	bool flag(const std::string& key) const;
	std::string argument(size_t index) const;

private:
	const char m_key_prefix = '-';

	std::unordered_map<std::string, std::string> m_options;
	std::unordered_set<std::string> m_flags;
	std::vector<std::string> m_arguments;
};

}
