#pragma once

#include <vector>

#include "format.hpp"

namespace kr {
namespace gfx {

struct VertexInput {
	enum class Rate { Vertex, Instance };

	struct BindingDesc {
		uint32_t binding;
		uint32_t stride;
		Rate rate;

		BindingDesc() {}
		BindingDesc(uint32_t binding, uint32_t stride, Rate rate)
			: binding(binding), stride(stride), rate(rate) {}
	};

	struct AttrDesc {
		uint32_t binding;
		uint32_t location;
		Format format;
		uint32_t offset;

		AttrDesc() {}
		AttrDesc(
			uint32_t binding,
			uint32_t loc,
			Format format,
			uint32_t offset
		) :
			binding(binding),
			location(loc),
			format(format),
			offset(offset)
		{}
	};

	std::vector<BindingDesc> bindings;
	std::vector<AttrDesc> attributes;
};

}}
