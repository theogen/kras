#include <unistd.h>
#include <istream>

#ifdef _WIN32
  #include <windows.h>
  #include <direct.h>
  #include <fileapi.h>
  #define GETCWD ::_getcwd
  #define CD ::_chdir
#else
  #include <linux/limits.h>
  #include <sys/types.h>
  #include <sys/stat.h>
  #define GETCWD ::getcwd
  #define CD ::chdir
#endif

#include "filesys.hpp"
#include "str.hpp"

namespace kr {

std::string Filesys::cwd()
{
	char temp[PATH_MAX];
	return GETCWD(temp, sizeof(temp)) ? std::string(temp) : std::string("");
}

bool Filesys::chdir(const std::string& dir)
{
	int res = CD(dir.c_str());
	return 0 == res;
}


bool Filesys::file_exists(const std::string& path)
{
	struct stat buffer;   
	return 0 == stat(path.c_str(), &buffer); 
}


bool Filesys::is_regular_file(const std::string& path)
{
  #ifdef _WIN32
	DWORD attr = GetFileAttributes(path.c_str());
	return 0 == (attr & FILE_ATTRIBUTE_DIRECTORY);
  #else
	struct stat s;
	if (0 == stat(path.c_str(), &s)) {
		return s.st_mode & S_IFREG;
	}
	return false;
  #endif
}

bool Filesys::is_directory(const std::string& path)
{
  #ifdef _WIN32
	DWORD attr = GetFileAttributes(path.c_str());
	return attr & FILE_ATTRIBUTE_DIRECTORY;
  #else
	struct stat s;
	if (0 == stat(path.c_str(), &s)) {
		return s.st_mode & S_IFDIR;
	}
	return false;
  #endif
}


std::string Filesys::basename(const std::string& path)
{
	int pos = path.length() - 1;

	// Skip trailing slashes.
	for (; pos >= 0; --pos)
		if (path[pos] != '/')
			break;
	int trailing = pos;

	// It's root directory.
	if (pos == -1)
		return "/";

	// Get location of last slash.
	for (; pos >= 0; --pos)
		if (path[pos] == '/')
			break;

	// Specified file is in current directory.
	if (pos == -1)
		return path.substr(0, trailing + 1);

	return path.substr(pos + 1, trailing - pos);
}

std::string Filesys::dirname(const std::string& path, bool trailing_slash)
{
	int pos = path.length() - 1;

	// Skip trailing slashes.
	for (; pos >= 0; --pos)
		if (path[pos] != '/')
			break;

	// It's root directory.
	if (pos == -1)
		return "/";

	// Get location of last slash.
	for (; pos >= 0; --pos)
		if (path[pos] == '/')
			break;

	// Skip trailing slashes ... again.
	for (; pos >= 0; --pos)
		if (path[pos] != '/') {
			++pos;
			break;
		}

	// Specified file is in current directory.
	if (pos == -1)
		return trailing_slash ? "./" : ".";

	return path.substr(0, pos) + (trailing_slash ? "/" : "");
}

std::string Filesys::suffix(const std::string& path, bool lower)
{
	// Writing the string from the end until a dot '.' is encountered.
	std::string res = "";
	for (unsigned i = path.length() - 1; i >= 0; --i) {
		if (path[i] != '.')
			res += path[i];
		else break;
	}

	// Lower the characters case.
	if (lower)
		res = Str::lower(res);

	return res;
}

std::string Filesys::remove_suffix(const std::string& path)
{
	size_t lastindex = path.find_last_of("."); 
	return path.substr(0, lastindex); 
}


std::vector<char>* Filesys::istream_to_char_arr(std::istream& in, size_t block_size)
{
	std::vector<char>* data = new std::vector<char>();

	while(in)
	{
		size_t current_size = data->size();
		data->resize(current_size + block_size);
		in.read(&*data->begin() + current_size, block_size);
		data->resize(current_size + in.gcount());
	}

	return data;
}

bool Filesys::mkdir(const std::string& path)
{
	int res;

#ifdef _WIN32
	res = _mkdir(path.c_str());
#else 
	res = ::mkdir(path.c_str(), 0777);
#endif

	return res == 0;
}

}
