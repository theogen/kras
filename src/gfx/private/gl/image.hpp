#pragma once

#include "opengl.h"
#include "../../image.hpp"

namespace kr {
namespace gfx {
namespace gl {

class Image : public gfx::Image {
public:
	Image(const Info& info);
	virtual ~Image();

	virtual uint32_t width() const noexcept override
	{ return info.width; }
	virtual uint32_t height() const noexcept override
	{ return info.height; }

	uint32_t id() const noexcept { return m_texture_id; }
	GLenum target() const noexcept { return m_target; }

private:
	static GLint get_gl_filter(Filter filter);

private:
	Info info;

	uint32_t m_texture_id;
	GLenum m_target;
};

}}}
