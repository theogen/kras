#include <algorithm>
#include <stdexcept>
#include <string.h>
#include "cli.hpp"

namespace kr {


CLI::CLI(int argc, const char** argv, const std::vector<std::string>& options)
{
	std::string key;
	bool isvalue = false;
	for (int i = 1; i < argc; ++i) {
		int len = strlen(argv[i]);
		// Option or flag.
		if (len > 1 && argv[i][0] == m_key_prefix)
		{
			if (isvalue) {
				throw std::runtime_error(key + ": a value is expected");
			}

			key.assign(argv[i] + 1, len - 1);

			// Option.
			if (std::find(options.begin(), options.end(), key) != options.end()) {
				isvalue = true;
				continue;
			}

			// Flag.
			m_flags.insert(key);
		}

		// Value or argument.
		std::string value = std::string(argv[i], len);
		if (isvalue) {
			isvalue = false;
			m_options[key] = value;
		}
		else {
			m_arguments.push_back(value);
		}
	}

	if (isvalue) {
		throw std::runtime_error(key + ": a value is expected");
	}
}

int CLI::arguments_count() const
{
	return m_arguments.size();
}

std::string CLI::option(const std::string& key)
{
	return m_options[key];
}

bool CLI::flag(const std::string& key) const
{
	auto iter = m_flags.find(key);
	return iter != m_flags.end();
}

std::string CLI::argument(size_t index) const
{
	if (index >= m_arguments.size()) {
		return "";
	}

	return m_arguments[index];
}


}
